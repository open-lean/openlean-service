import {DefaultSequence} from './sequence';
import {createUseCaseBinding, UseCaseOptions} from './core/decorators';
import {BaseApplication, IUseCase} from './core/abstracts';
import {LogErrorMiddlewareProvider} from './core/middlewares/log-error.middleware';
import {JobManager, QueueBindings, QueueComponent, QueueConfig} from './core/configs/queue';
import {AuthenticationComponent} from '@loopback/authentication';
import {AuthorizationComponent, AuthorizationDecision, AuthorizationTags} from '@loopback/authorization';
import {Binding} from '@loopback/boot';
import {Provider, DynamicValueProviderClass, BindingFromClassOptions} from '@loopback/context';
import {ApplicationConfig, createBindingFromClass, Constructor, Component} from '@loopback/core';
import {HealthComponent, HealthBindings} from '@loopback/extension-health';
import {MetricsComponent, MetricsBindings} from '@loopback/extension-metrics';
import {Class} from '@loopback/repository';
import {HttpRequestListener, RestServer} from '@loopback/rest';
import {getJobDefinition, queueSettings} from './core/configs/queue/auth.queue';
import addingLiveAndReadyCheck from './health-check';
import Settings from './settings';
import {RestExplorerBindings, RestExplorerComponent} from '@loopback/rest-explorer';
import path from 'path';
import {
  PasswordHasherBindings,
  TokenServiceBindings,
  TokenServiceConstants,
  AuthorizationProvider,
  JwtService,
  BcryptHasher,
  AuthService,
  AuthTokenService,
  AuthorizationService
} from './modules/auth';
import {ServiceBindings} from './keys';
import {AuthComponent} from './modules/auth/auth.component';
import {UserComponent} from './modules/user';
import MailService from './services/mail/mail.service';
import {registerJobBindings} from "./modules/user/jobs";

export {ApplicationConfig};

export class AuthApplication extends BaseApplication {
  constructor(options: ApplicationConfig = {}) {
    super(options);
    this.projectRoot = __dirname;

    this.setupBindings();

    this.setupComponents();

    this.sequence(DefaultSequence);

    this.setupMiddleware();

    this.setupMonitorComponents();
    // Set up default home page
    this.static('/', path.join(__dirname, '../public'));

    this.bootOptions = {
      controllers: {
        glob: '/modules/*/controllers/*@(.controller.js)'
      },
      repositories: {
        glob: '/core/repositories/*@(.repository.js)'
      },
      datasources: {
        glob: '/core/datasources/*@(.datasource.js)'
      }
    };
  }

  protected setupComponents(): void {
    this.component(AuthenticationComponent);

    const authorizorBinding = this.component(AuthorizationComponent);
    this.configure(authorizorBinding.key).to({
      precedence: AuthorizationDecision.ALLOW
    });

    this.bind('security.authorization.provider')
      .toProvider(AuthorizationProvider)
      .tag(AuthorizationTags.AUTHORIZER);

    this.configure(QueueBindings.COMPONENT).to({
      queues: queueSettings,
      getJobDefinitionFn: getJobDefinition
    } as QueueConfig);
    this.component(QueueComponent);

    if (process.env.NODE_ENV !== 'production') {
      // Customize @loopback/rest-explorer configuration here
      this.configure(RestExplorerBindings.COMPONENT).to({
        path: '/explorer'
      });
      this.component(RestExplorerComponent);
    }
  }

  protected setupBindings(): void {
    registerJobBindings(this);
    // Bind bcrypt hash services
    this.bind(PasswordHasherBindings.ROUNDS).to(10);
    this.bind(PasswordHasherBindings.PASSWORD_HASHER).toClass(BcryptHasher);

    this.bind(TokenServiceBindings.TOKEN_SECRET).to(
      Settings.TOKEN_SECRET
    );

    this.bind(TokenServiceBindings.TOKEN_EXPIRES_IN).to(
      TokenServiceConstants.TOKEN_EXPIRES_IN_VALUE
    );

    // Service bindings
    this.service(AuthTokenService, {name: ServiceBindings.AUTH_TOKEN_SERVICE});
    this.service(AuthorizationService, {name: ServiceBindings.AUTHORIZATION_SERVICE});
    this.service(JwtService, {name: ServiceBindings.JWT_SERVICE});
    this.service(AuthService, {name: ServiceBindings.ACCOUNT_SERVICE});
    this.service(MailService);
    this.component(AuthComponent);
    this.component(UserComponent);

  }

  protected setupMiddleware(): void {
    this.add(createBindingFromClass(LogErrorMiddlewareProvider));
  }

  setupMonitorComponents(): void {
    this.component(HealthComponent);
    this.configure(HealthBindings.COMPONENT).to({
      healthPath: '/health',
      livePath: '/live',
      readyPath: '/ready'
    });

    this.component(MetricsComponent);
    this.configure(MetricsBindings.COMPONENT).to({
      endpoint: {
        basePath: '/metrics'
      },
      defaultMetrics: {
        timeout: 5000
      }
    });

    addingLiveAndReadyCheck(this);
  }

  get restServer(): RestServer {
    return this.getSync<RestServer>('servers.RestServer');
  }

  get requestHandler(): HttpRequestListener {
    return this.restServer.requestHandler;
  }

  async exportOpenApiSpec(outFile = '', log = console.log): Promise<void> {
    return this.restServer.exportOpenApiSpec(outFile, log);
  }

  start = async (): Promise<void> => {
    console.log('Auth application is starting...');
    await super.start();
    console.log('Auth application is started...');
  };

  /**
   * Add a component to this application. Also mounts
   * all the components' repositories.
   */
  component<T extends Component>(
    componentCtor: Constructor<T>,
    nameOrOptions?: string | BindingFromClassOptions
  ): Binding<T> {
    const output = super.component(componentCtor, nameOrOptions);
    this.mountComponentRepositories(componentCtor);
    this.mountUseCases(componentCtor);
    return output;
  }

  mountComponentRepositories(component: Class<Component>) {
    if (component.name) {
      const componentKey = `components.${component.name}`;
      const compInstance: Component = this.getSync(componentKey);

      // register a component's repositories in the app
      if (compInstance.repositories) {
        for (const repo of compInstance.repositories) {
          this.repository(repo);
        }
      }
    }
  }

  /**
   * Add a use case to this application.
   *
   * @param cls - The use case or provider class
   * @param nameOrOptions
   * @see Similar to service document
   */
  public useCase<U extends IUseCase<any>>(
    cls: UseCaseOrProviderClass<U>,
    nameOrOptions?: string | UseCaseOptions
  ): Binding<U> {
    const options = toOptions(nameOrOptions);
    const binding = createUseCaseBinding(cls, options);
    this.add(binding);
    return binding;
  }

  mountUseCases(component: Class<Component>) {
    if (component.name) {
      const componentKey = `components.${component.name}`;
      const compInstance: Component = this.getSync(componentKey);

      // register a component's use cases in the app
      if (compInstance.useCases) {
        for (const useCase of compInstance.useCases) {
          this.useCase(useCase);
        }
      }
    }
  }
}

export type UseCaseOrProviderClass<T = any> = Constructor<T | Provider<T>> | DynamicValueProviderClass<T>;

/**
 * Normalize name or options to `BindingFromClassOptions`
 * @param nameOrOptions - Name or options for binding from class
 */
function toOptions(nameOrOptions?: string | BindingFromClassOptions) {
  if (typeof nameOrOptions === 'string') {
    return {name: nameOrOptions};
  }
  return nameOrOptions ?? {};
}
