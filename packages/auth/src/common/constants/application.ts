import {RoleTypes} from './enum';

export const AUTH_SERVICE_TYPE = 1;
export const ADMIN_ROLES = [RoleTypes.KT_ADMIN, RoleTypes.KT_MANAGER];
export enum PermissionType {
  COMPANY_OWNER = 'CompanyOwner',
  COMPANY_ADMIN = 'CompanyAdmin',
  COMPANY_USER = 'CompanyUser',
  PLANT_CONFIG_READ = 'PlantConfigRead',
  PLANT_CONFIG_WRITE = 'PlantConfigWrite',
  PLANT_ANALYTIC = 'PlantAnalytic'
}

export class SystemConstant {
  static readonly SERVICE_TYPE: number = AUTH_SERVICE_TYPE;
  static readonly ALL_USER_PERMS: string[] = [
    PermissionType.COMPANY_OWNER,
    PermissionType.COMPANY_ADMIN,
    PermissionType.COMPANY_USER,
    PermissionType.PLANT_CONFIG_READ,
    PermissionType.PLANT_CONFIG_WRITE,
    PermissionType.PLANT_ANALYTIC
  ];
  static readonly COMPANY_FULL_PERMS: string[] = [
    PermissionType.COMPANY_OWNER,
    PermissionType.COMPANY_ADMIN
  ];
  static readonly ALL_ROLES: string[] = [
    RoleTypes.USER,
    RoleTypes.USER_APP,
    RoleTypes.KT_ADMIN,
    RoleTypes.KT_MANAGER
  ];
  static readonly MS_PER_DAY = 86400000; // 1000 * 60 * 60 * 24;
  static readonly MS_PER_WEEK = 604800000; // SystemConstant.MS_PER_DAY * 7;
  static readonly MS_REDIS_TTL = 3600000; // 60* 60* 1000;
}

export class ValidationConstant {
  static readonly MAX_PASSWORD_LENGTH: number = 60;
  static readonly MIN_PASSWORD_LENGTH: number = 6;
}