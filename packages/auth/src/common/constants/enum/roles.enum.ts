export enum RoleTypes {
  USER = 'user',
  USER_APP = 'user_app',
  KT_ADMIN = 'kt_admin',
  KT_MANAGER = 'kt_manager',
  KT_ADMIN_READ = 'kt_admin_read'
}
