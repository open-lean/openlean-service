export enum APIScopes {
  INTERNAL_AUTH_API = 'internal_auth_api',
  INTERNAL_ADMIN_API = 'internal_admin_api',
  INTERNAL_CONFIG_API = 'internal_config_api',
  INTERNAL_ANALYTIC_API = 'internal_analytic_api',
  INTERNAL_COLLECTOR_API = 'internal_collector_api',
  EXTERNAL_PUBLIC_API = 'external_public_api',

  // Scope to reset user password
  AUTH_RESET_PASSWORD = 'auth_reset_password'
}
