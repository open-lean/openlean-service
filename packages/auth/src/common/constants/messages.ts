import {ValidationConstant} from './application';

export function formatMsg(msg: string, ...args: any[]) {
  for (const k in args) {
    msg = msg.replace(new RegExp('\\{' + k + '\\}', 'g'), args[k]);
  }
  return msg;
}

export namespace ErrorMsg {
  export const IS_INVALID = (field: string) => formatMsg('{0} is invalid', [field]);
  export const IS_REQUIRED = (field: string) => formatMsg('{0} is required', [field]);
  export const IS_DISABLED = (field: string) => formatMsg('{0} is disabled', [field]);
  export const IS_EMPTY = (field: string) => formatMsg('{0} is empty', [field]);
  export const NOT_EXIST = (field: string) => formatMsg('{0} does not exist', [field]);
  export const NOT_BELONG_TO = (params: string[]) => formatMsg('{0} does not belong to {1}', params);
  export const BEING_USED = (field: string) => formatMsg('{0} is being used');
  export const NO_PERMISSION = "You don't have permission.";
  export const IS_INCORRECT = (field: string) => formatMsg('Your {0} is incorrect, please check again!', [field]);
  export const IS_DEACTIVATED = (field: string) => formatMsg('{0} is deactivated', [field]);
  export const NOT_ALLOW = 'Not Allow'
}

export namespace QueueMsg {
  export const JOB_NOT_DEFINED = (job: string) => formatMsg('Job "{0}" is not defined', [job]);
}

export namespace AccountMsg {
  export const INVALID_EMAIL_PASSWORD = 'Invalid email or password.';
  export const NOT_ACTIVATED = 'Your account has not been activated.';
  export const DISABLED = 'Your account has been disabled.';
  export const INVALID_CURRENT_PASSWORD = 'Invalid current password.';
  export const PASSWORD_TOO_LONG = (len: number) => formatMsg(
    'The password entered was too long. Max length is {0} (entered {1})', [ValidationConstant.MAX_PASSWORD_LENGTH, len]
  );
  export const PASSWORD_TOO_SHORT = (len: number) => formatMsg(
    'The password entered was too short. Min length is {0} (entered {1})', [ValidationConstant.MIN_PASSWORD_LENGTH, len]
  )
}

export namespace AccessTokenMsg {
  export const TTL_NOT_BE_ZERO = 'token.ttl must be not be 0';
  export const TTL_GREATER_MINUS_ONE = 'token.ttl must be >= -1';
  export const INVALID_TOKEN = 'Invalid Access Token';
}
