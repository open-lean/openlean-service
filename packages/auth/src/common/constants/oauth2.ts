export class OAuth2Constant {
  public static readonly CLIENT_APPLICATION_TYPES: string[] = ['public', 'confidential'];
  public static readonly AUTH_METHODS: string[] = ['none', 'client_secret_post', 'client_secret_basic'];
  public static readonly GRANT_TYPES: string[] = ['authorization_code', 'implicit', 'client_credentials', 'password'];
  public static readonly RESPONSE_TYPES: string[] = ['code', 'token'];
  public static readonly TOKEN_TYPES: string[] = ['bearer', 'jwt', 'mac'];
}
