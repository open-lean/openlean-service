
import {ErrorCode, HttpStatus} from './keys';
import HttpErrors from 'http-errors';


export class KTException extends Error {
  public statusCode: number;
  public code?: number;

  constructor(msg: string, statusCode?: number, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.VALIDATION_ERROR;
    }
    if (!statusCode) {
      statusCode = HttpStatus.BAD_REQUEST;
    }
    const error = new HttpErrors[statusCode](msg);
    this.message = error.message;
    this.name = error.name;
    this.statusCode = error.statusCode;
    this.code = code;
  }
}

export class BadRequestException extends HttpErrors.BadRequest {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class UnauthorizedException extends HttpErrors.Unauthorized {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.PERMISSION_DENIED
    }
    this.code = code;
  }
}
export class ValidationException extends KTException {
  constructor(msg: string, code: number = ErrorCode.VALIDATION_ERROR) {
    super(msg, HttpStatus.BAD_REQUEST, code);
    this.name = 'ValidationError';
  }
}

// PaymentRequired

export class ForbiddenException extends HttpErrors.Forbidden {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.ACCESS_DENIED
    }
    this.code = code;
  }
}

export class NotFoundException extends HttpErrors.NotFound {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.RESOURCE_NOT_FOUND
    }
    this.code = code;
  }
}

export class MethodNotAllowedException extends HttpErrors.MethodNotAllowed {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class ProxyAuthenticationRequiredException extends HttpErrors.ProxyAuthenticationRequired {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class RequestTimeoutException extends HttpErrors.RequestTimeout {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class ConflictException extends HttpErrors.Conflict {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.VALIDATION_ERROR
    }
    this.code = code;
  }
}

// Gone
// LengthRequired
// PreconditionFailed
// MisdirectedRequest

export class PayloadTooLargeException extends HttpErrors.PayloadTooLarge {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}
export class URITooLongException extends HttpErrors.URITooLong {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}
export class UnsupportedMediaTypeException extends HttpErrors.UnsupportedMediaType {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

// RangeNotSatisfiable
// ExpectationFailed
// ImATeapot

export class UnprocessableEntityException extends HttpErrors.UnprocessableEntity {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.VALIDATION_ERROR
    }
    this.code = code;
  }
}

// Locked
// FailedDependency
// UnorderedCollection
// UpgradeRequired
// PreconditionRequired
// TooManyRequests
// RequestHeaderFieldsTooLarge
// UnavailableForLegalReasons

export class InternalServerErrorException extends HttpErrors.InternalServerError {
  constructor(msg: string, code?: number) {
    super(msg);
    if (!code) {
      code = ErrorCode.INTERNAL_ERROR
    }
    this.code = code;
  }
}


export class NotImplementedException extends HttpErrors.NotImplemented {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class BadGatewayException extends HttpErrors.BadGateway {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class ServiceUnavailableException extends HttpErrors.ServiceUnavailable {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

export class GatewayTimeoutException extends HttpErrors.GatewayTimeout {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

// HTTPVersionNotSupported
// VariantAlsoNegotiates
// InsufficientStorage
// LoopDetected

export class BandwidthLimitExceededException extends HttpErrors.BandwidthLimitExceeded {
  constructor(msg: string, code?: number) {
    super(msg);
    this.code = code;
  }
}

// NotExtended
// NetworkAuthenticationRequire





