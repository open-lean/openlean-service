import fs from 'fs';
import path from 'path';
import winston, {format} from 'winston';
import {LogData, ElasticsearchTransport} from 'winston-elasticsearch';
import debugFormat from 'winston-format-debug';
import DailyRotateFile from 'winston-daily-rotate-file';
import elasticsearchTemplate from '../templates/elasticsearch';
import esFormat from '../format/es';
import requestFormat from '../format/request';
import prefixFormat from '../format/prefix';
import Settings from '../../../settings';

export const LEVELS: {[key: string]: number} = {
  error: 0,
  warn: 1,
  info: 2,
  debug: 3
};

const COLORS: {[key: string]: string | string[]} = {
  error: 'red',
  warn: 'yellow',
  info: 'green',
  debug: 'cyan'
};

const LOG_DIR = process.env.LOG_DIR ?? path.resolve(__dirname, '../../../../logs/');
// Create the directory if it does not exist
if (!fs.existsSync(LOG_DIR)) {
  fs.mkdirSync(LOG_DIR);
}

winston.addColors(COLORS);

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        requestFormat({debug: true, colors: true}),
        prefixFormat(),
        winston.format.colorize({message: true}),
        debugFormat({
          levels: LEVELS,
          colors: COLORS,
          colorizeMessage: false // Already colored by `winston.format.colorize`.
        })
      )
    }),
    new DailyRotateFile({
      dirname: LOG_DIR,
      filename: Settings.LOGGER_FILE_NAME,
      datePattern: 'YYYY-MM-DD',
      zippedArchive: false,
      maxSize: '20m',
      maxFiles: '30d',
      json: true,
      handleExceptions: true,
      level: 'info',
      format: format.combine(
        requestFormat({debug: true}),
        format.timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
        format.json()
      )
    })
  ]
});

// Set ELASTICSEARCH_URLS to a list of URLs to connect to, e.g. "http://elasticsearch:9200".
const ELASTICSEARCH_URLS = Settings.ELASTICSEARCH_URLS;

if (ELASTICSEARCH_URLS) {
  const nodes = ELASTICSEARCH_URLS.split(',');

  // winston-elasticsearch automatically moves a bunch of the log data into
  // a field called "meta".  This undoes this and moves it all back...
  function esTransformer({message, level, timestamp, meta}: LogData) {
    return {message, level, timestamp, ...meta};
  }

  const esTransport = new ElasticsearchTransport({
    transformer: esTransformer,
    clientOpts: {
      auth: {
        username: Settings.LOGGER_ELASTICSEARCH_USERNAME,
        password: Settings.LOGGER_ELASTICSEARCH_PASSWORD
      },
      nodes
    } as any,
    index: Settings.LOGGER_ELASTICSEARCH_INDEX,
    indexTemplate: elasticsearchTemplate,
    ensureIndexTemplate: true,
    format: winston.format.combine(requestFormat({debug: false}), esFormat())
  });

  logger.add(esTransport);
}

export default logger;
