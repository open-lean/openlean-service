export class FormatterUtil {
  /**
   * To format text as format {{key}} by the given data object
   *
   * @param [text] The text that needs to format
   * @param [data] The data object as <key,value> collection
   * @return a formatted text
   */
  static formatTextByDataObject(text: string, data: {[key: string]: any}): string {
    for (const key of Object.keys(data)) {
      text = text.replace(new RegExp('{{' + key + '}}', 'g'), data[key]);
    }
    return text;
  }

  /**
   * To format text as format {{0}} by the given arguments as array
   *
   * @param [text] The text that needs to format
   * @param [args] The list of value to replace
   * @return a formatted text
   */
  static formatTextByArgs(text: string, ...args: any[]): string {
    for (const index in args) {
      text = text.replace(new RegExp('{{' + index + '}}', 'g'), args[index]);
    }
    return text;
  }
}
