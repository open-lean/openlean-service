import {normalizeList} from './util';

/**
 * Normalize scope to string[]
 * @param {String|String[]} scope
 * @returns {String[]}
 */
export function normalizeScope(scope: string | string[]): string[] {
  return normalizeList(scope);
}

/**
 * Check if one of the scopes is in the allowedScopes array
 * @param {String[]} allowedScopes An array of required scopes
 * @param {String[]} tokenScopes An array of granted scopes
 * @returns {boolean}
 */
export function isScopeAllowed(allowedScopes: string | string[], tokenScopes: string | string[]) {
  allowedScopes = normalizeScope(allowedScopes);
  tokenScopes = normalizeScope(tokenScopes);
  if (allowedScopes.length === 0) {
    return true;
  }
  for (let i = 0, n = allowedScopes.length; i < n; i++) {
    if (tokenScopes.indexOf(allowedScopes[i]) !== -1) {
      return true;
    }
  }
  return false;
}

/**
 * Check if the requested scopes are covered by authorized scopes
 * @param {String|String[]} requestedScopes
 * @param {String|String[]} authorizedScopes
 * @returns {boolean}
 */
export function isScopeAuthorized(requestedScopes: string | string[], authorizedScopes: string | string[]) {
  requestedScopes = normalizeScope(requestedScopes);
  authorizedScopes = normalizeScope(authorizedScopes);
  if (requestedScopes.length === 0) {
    return true;
  }
  for (let i = 0, n = requestedScopes.length; i < n; i++) {
    if (authorizedScopes.indexOf(requestedScopes[i]) === -1) {
      return false;
    }
  }
  return true;
}
