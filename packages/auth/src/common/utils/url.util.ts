import * as Process from 'process';
import Settings from '../../settings';

/**
 * To get webapp URL by the given client ID
 *
 * @param [clientId]: The client ID in OAuthClientApplication.
 */
export function getServiceURLByClientId(clientId: string): string {
  let serviceURL = Settings.base.analyticWebUrl;
  if (clientId === Process.env.CONFIG_CLIENT_ID) {
    serviceURL = Settings.base.configWebUrl;
  }
  return serviceURL;
}

/**
 * To build login URL after successfully login via saml request
 *
 * @param [clientId]: The client ID in OAuthClientApplication.
 * @param token: The user's token that login via saml
 */
export function buildLoginURLWithToken(clientId: string, token: string): string {
  const serviceURL = getServiceURLByClientId(clientId);
  return `${serviceURL}/login?token=${token}`;
}

/**
 * To build login URL with error message via saml request
 * @param [clientId]: The client ID in OAuthClientApplication.
 * @param errMsg: The error message
 */
export function buildLoginURLWithErrMsg(clientId: string, errMsg: string): string {
  const serviceURL = getServiceURLByClientId(clientId);
  return `${serviceURL}/login?errorMessage=${errMsg}`;
}
