import {User} from '../../core/models';
import {BadRequestException, UnprocessableEntityException} from '../exception';
import {AccountMsg, ErrorMsg} from '../constants/messages';
import {ErrorCode} from '../exception/keys';
import isemail from 'isemail';

export function isActiveUser(user?: User | null): boolean {
  if (!user) {
    throw new UnprocessableEntityException(ErrorMsg.NOT_EXIST('User'), ErrorCode.RESOURCE_NOT_FOUND);
  }
  if (!user.emailVerified) {
    throw new BadRequestException(AccountMsg.NOT_ACTIVATED);
  }
  return true;
}

export function isValidEmail(email: string): boolean {
  if (!isemail.validate(email)) {
    throw new UnprocessableEntityException(ErrorMsg.IS_INVALID('email'));
  }
  return true;
}