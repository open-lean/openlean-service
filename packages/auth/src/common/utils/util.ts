import _ from 'lodash';
import {KeyPrefix} from '../../keys';

export function getServiceKey(name: string) {
  return KeyPrefix.SERVICE + name;
}

/**
 * Compare 2 given times
 * @param timeA: format as HH:mm:ss
 * @param timeB: format as HH:mm:ss
 *
 * return: -1: timeA < timeB, 0: timeA == timeB, 1: timeA > timeB
 */
export function compareTime(timeA: string, timeB: string): number {
  const dateA = new Date(`1/1/2020 ${timeA}`);
  const dateB = new Date(`1/1/2020 ${timeB}`);
  let result = 0;
  if (dateA > dateB) {
    result = 1;
  } else if (dateA < dateB) {
    result = -1;
  }
  return result;
}

export function compareDate(dateA: Date, dateB: Date): number {
  let result = 0;
  if (dateA > dateB) {
    result = 1;
  } else if (dateA < dateB) {
    result = -1;
  }
  return result;
}

export function delay(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

// Apply the mixins into the base class via the JS at runtime
export function applyMixins(derivedCtor: any, constructors: any[]) {
  constructors.forEach(baseCtor => {
    Object.getOwnPropertyNames(baseCtor.prototype).forEach(name => {
      Object.defineProperty(
        derivedCtor.prototype,
        name,
        Object.getOwnPropertyDescriptor(baseCtor.prototype, name) as any
      );
    });
  });
}

export function formatTextWithData(text: string, data: any) {
  for (const key in data) {
    const re = RegExp(`{${key}}`, 'gi');
    text = text.replace(re, data[key]);
  }
  return text;
}

export function normalizeFields(includeFields: string[], excludeFields: string[] = []): any {
  const output: any = {};
  for (const field of includeFields) {
    output[field] = true;
  }
  for (const field of excludeFields) {
    output[field] = false;
  }
  return output;
}

export function convertDateToString(date: Date): string {
  return date.toISOString().slice(0, 19).replace('T', ' ');
}

/**
 * To get value from object by the given pattern
 * @param object: The object, example: {name: {firstName: "aa"}}
 * @param pattern: The pattern, example: "name.firstName"
 */
export function getValueByPattern(object: {[key: string]: unknown}, pattern: string): any | undefined {
  let output: {[key: string]: unknown} | undefined = undefined;
  pattern = pattern.trim();
  if (_.isObject(object) && Boolean(pattern)) {
    const fields = pattern.split('.');
    const first = fields.shift() as string;
    output = object[first] as any;
    for (const field of fields) {
      if (!_.isObject(output)) {
        output = undefined;
        break;
      }
      output = output[field] as any;
    }
  }
  return output;
}

/**
 * To set value to object by the given pattern
 * @param object: The object, example: {name: {firstName: "aa"}}
 * @param pattern: The pattern, example: "name.firstName"
 * @param value: The value, example: "abc"
 */
export function setValueByPattern(object: {[key: string]: unknown}, pattern: string, value: any) {
  pattern = pattern.trim();
  if (_.isObject(object) && Boolean(pattern)) {
    const fields = pattern.split('.') as string[];
    const lastField = fields.pop() as string;
    let variable: {[key: string]: unknown} | undefined = object;
    for (const field of fields) {
      if (!_.isObject(variable)) {
        variable = undefined;
        break;
      }
      variable = variable[field] as any;
    }
    if (variable !== undefined && Boolean(lastField)) {
      variable[lastField] = value;
    }
  }
}

export function safeInt(value: any, defaultValue?: number): number {
  defaultValue = defaultValue ?? 0;
  let output = parseInt(value);
  if (isNaN(output)) {
    output = defaultValue;
  }
  return output;
}

export function take(obj: any, property: any, defaultValue?: any): any {
  let value = _.get(obj, property);
  if (value !== undefined) {
    _.unset(obj, property);
  } else if (defaultValue !== undefined) {
    value = defaultValue;
  }
  return value;
}

/**
 * Normalize items to string[]
 * @param {String|String[]} items
 * @returns {String[]}
 */
export function normalizeList(items: string | string[]): string[] {
  if (!items) {
    return [];
  }
  let list;
  if (Array.isArray(items)) {
    list = Object.assign([], items);
  } else {
    list = items.split(/[\s,]+/g).filter(Boolean);
  }
  return list;
}
