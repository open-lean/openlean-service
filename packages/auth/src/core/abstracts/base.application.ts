import {Binding, BootMixin} from '@loopback/boot';
import {
  ApplicationConfig,
  BindingFromClassOptions,
  Component,
  Constructor,
  DynamicValueProviderClass,
  Provider
} from '@loopback/core';
import {HealthBindings, HealthComponent} from '@loopback/extension-health';
import {MetricsBindings, MetricsComponent} from '@loopback/extension-metrics';
import {Class, RepositoryMixin} from '@loopback/repository';
import {RestApplication} from '@loopback/rest';
import {ServiceMixin} from '@loopback/service-proxy';
import {createUseCaseBinding, UseCaseOptions} from '../decorators';
import {DefaultSequence} from '../../sequence';
import {IUseCase} from './types';

export class BaseApplication extends BootMixin(ServiceMixin(RepositoryMixin(RestApplication))) {
  constructor(options: ApplicationConfig = {}) {
    super(options);

    this.setupBindings();
    this.setupComponents();
    this.setupSequence();
    this.setupMiddleware();

    // Customize @loopback/boot Booter Conventions here
    this.bootOptions = {
      controllers: {
        // Customize ControllerBooter Conventions here
        dirs: ['controllers'],
        extensions: ['.controller.js'],
        nested: true
      }
    };
    this.setupMonitorComponents();
  }

  protected setupBindings(): void {}

  protected setupSequence(): void {
    // Set up the custom sequence
    this.sequence(DefaultSequence);
  }

  protected setupComponents(): void {}

  protected setupMiddleware(): void {}

  protected setupMonitorComponents(): void {
    this.component(HealthComponent);
    this.configure(HealthBindings.COMPONENT).to({
      healthPath: '/health',
      livePath: '/live',
      readyPath: '/ready'
    });

    this.component(MetricsComponent);
    this.configure(MetricsBindings.COMPONENT).to({
      endpoint: {
        basePath: '/metrics'
      },
      defaultMetrics: {
        timeout: 5000
      }
    });
  }

  protected async bindingComponentKeysAfterStarting() {}

  start = async (): Promise<void> => {
    await super.start();

    await this.bindingComponentKeysAfterStarting();
  };

  /**
   * Add a component to this application. Also mounts
   * all the components' repositories.
   */
  component<T extends Component>(
    componentCtor: Constructor<T>,
    nameOrOptions?: string | BindingFromClassOptions
  ): Binding<T> {
    const output = super.component(componentCtor, nameOrOptions);
    this.mountComponentRepositories(componentCtor);
    this.mountUseCases(componentCtor);
    return output;
  }

  mountComponentRepositories(component: Class<Component>) {
    if (component.name) {
      const componentKey = `components.${component.name}`;
      const compInstance: Component = this.getSync(componentKey);

      // register a component's repositories in the app
      if (compInstance.repositories) {
        for (const repo of compInstance.repositories) {
          this.repository(repo);
        }
      }
    }
  }

  /**
   * Add a use case to this application.
   *
   * @param cls - The use case or provider class
   * @param nameOrOptions
   * @see Similar to service document
   */
  public useCase<U extends IUseCase<any>>(
    cls: UseCaseOrProviderClass<U>,
    nameOrOptions?: string | UseCaseOptions
  ): Binding<U> {
    const options = toOptions(nameOrOptions);
    const binding = createUseCaseBinding(cls, options);
    this.add(binding);
    return binding;
  }

  mountUseCases(component: Class<Component>) {
    if (component.name) {
      const componentKey = `components.${component.name}`;
      const compInstance: Component = this.getSync(componentKey);

      // register a component's use cases in the app
      if (compInstance.useCases) {
        for (const useCase of compInstance.useCases) {
          this.useCase(useCase);
        }
      }
    }
  }
}

export type UseCaseOrProviderClass<T = any> = Constructor<T | Provider<T>> | DynamicValueProviderClass<T>;

/**
 * Normalize name or options to `BindingFromClassOptions`
 * @param nameOrOptions - Name or options for binding from class
 */
function toOptions(nameOrOptions?: string | BindingFromClassOptions) {
  if (typeof nameOrOptions === 'string') {
    return {name: nameOrOptions};
  }
  return nameOrOptions ?? {};
}
