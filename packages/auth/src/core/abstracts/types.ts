/**
 * UseCase interface
 */
export interface IUseCase<T> {
  invoke(...params: any): Promise<T>;
}
