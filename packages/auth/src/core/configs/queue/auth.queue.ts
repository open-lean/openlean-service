import {JobDefinition, QueueConstant, QueueSetting} from './';

enum QueueName {
  AUTH = 'openlean_auth_queue',
  NOTIFICATION = 'openlean_notification_queue'
}

enum ChannelName {
  DEFAULT = '__default__',
  AUTH = 'auth_channel'
}

export const queueSettings: QueueSetting[] = [
  {
    name: QueueName.AUTH,
    channels: [{name: ChannelName.DEFAULT, concurrency: 3}, {name: ChannelName.AUTH}]
  },
  {
    name: QueueName.NOTIFICATION,
    // Leave the empty channels that means we don't listen to any channels of the queue.
    // So we don't handle any messages from the queue.
    channels: []
  }
];

const jobDefinitions: any = {
  sendMailJob: (params: any) =>
    ({
      title: `SendMailJob to: ${params.to}`,
      jobOpts: {
        delay: 3000,
        attempts: QueueConstant.DEFAULT_QUEUE_ATTEMPTS,
        timeout: QueueConstant.DEFAULT_QUEUE_TIMEOUT,
        removeOnComplete: true
      },
      routingKey: 'send-mail',
      queueName: QueueName.AUTH,
      channel: ChannelName.DEFAULT
    } as JobDefinition)
};

export function getJobDefinition(jobName: string, parameters: any): JobDefinition | null {
  const jobOptions = jobDefinitions[jobName](parameters);
  if (jobOptions) {
    return {jobName, ...jobOptions};
  }
  return null;
}
