export namespace QueueConstant {
  export const DEFAULT_QUEUE_DELAY = 3000; // 3s
  export const DEFAULT_QUEUE_ATTEMPTS = 2;
  export const DEFAULT_QUEUE_TIMEOUT = 600000; // 10 minutes
  export const DEFAULT_CONCURRENCY = 3; // 3 concurrency
}

export namespace QueuePriority {
  export const SUPER_HIGHEST = 1;
  export const HIGHEST = 2;
  export const SUPER_HIGH = 3;
  export const HIGH = 4;
  export const ALARMING = 5;
  export const NORMAL = 6;
  export const LOW = 7;
  export const SUPER_LOW = 8;
  export const LOWEST = 9;
  export const SUPER_LOWEST = 10;
}

export enum JobStatus {
  PENDING = 'pending',
  ACTIVE = 'active',
  PAUSED = 'paused',
  ERROR = 'error',
  DONE = 'done'
}

export namespace JobConstant {
  export const SEND_MAIL_JOB = 'sendMailJob';
}
