import {FormatterUtil} from '../../../../common/utils';

export namespace QueueMsg {
  export const JOB_NOT_DEFINED = (job: string) => FormatterUtil.formatTextByArgs('Job "{0}" is not defined', [job]);
}
