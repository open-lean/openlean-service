import {BindingSpec, injectable, Binding, extensionFor, BindingScope} from '@loopback/core';
import {QueueBindings} from '../keys';
import {QUEUE_JOB_HANDLER} from '../types';

/**
 * A `BindingTemplate` function to configure the binding as a queue job.
 *
 * @param binding - Binding object
 */
export function asQueueJob<T = unknown>(binding: Binding<T>) {
  return binding
    .apply(extensionFor(QUEUE_JOB_HANDLER))
    .tag({namespace: QueueBindings.QUEUE_JOB_NAMESPACE})
    .inScope(BindingScope.SINGLETON);
}

/**
 * `@queueJob` decorates a queue job provider class
 *
 * @example
 * ```ts
 * @queueJob()
 * class QueueJobProvider implements Provider<QueueJob> {
 *   constructor(@config() private jobConfig: QueueJobConfig = {}) {}
 *   value() {
 *     // ...
 *   }
 * }
 * ```
 * @param specs - Extra binding specs
 */
export function queueJob(...specs: BindingSpec[]) {
  return injectable(asQueueJob, ...specs);
}
