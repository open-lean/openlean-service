import {BindingKey} from '@loopback/core';
import {QueueComponent} from './queue.component';
import {QueueConfig, RedisQueue} from './types';

/**
 * Binding keys used by this component.
 */
export namespace QueueBindings {
  /**
   * Binding key for `QueueComponent`
   */
  export const COMPONENT = BindingKey.create<QueueComponent>('components.QueueComponent');

  export const CONFIG = BindingKey.buildKeyForConfig<QueueConfig>(COMPONENT.key);

  export const QUEUE_MAPPING = BindingKey.create<{[key: string]: RedisQueue}>('queue.mapping');

  /**
   * Namespace for queue jobs
   */
  export const QUEUE_JOB_NAMESPACE = 'queue.jobs';
}
