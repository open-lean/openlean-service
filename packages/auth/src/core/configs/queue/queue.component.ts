import {AuthApplication} from '../../../application';
import {getLoggerFromFilename} from '../../../common/logging';
import {
  BindingScope,
  Component,
  ContextTags,
  extensionPoint,
  extensions,
  Getter,
  LifeCycleObserver,
  inject,
  CoreBindings
} from '@loopback/core';
import {QueueOptions, Job} from 'bull';
import {QueueBindings} from './keys';
import {JobManager} from './services';
import {QUEUE_JOB_HANDLER, QueueJob, QueueConfig, RedisQueue} from './types';

const logger = getLoggerFromFilename(__filename, {tags: ['redis', 'queue', 'job']});

@extensionPoint(QUEUE_JOB_HANDLER, {
  tags: {[ContextTags.KEY]: QueueBindings.COMPONENT},
  scope: BindingScope.SINGLETON
})
export class QueueComponent implements Component, LifeCycleObserver {
  services = [JobManager];

  private readonly jobMapping: {[key: string]: QueueJob} = {};
  private readonly queueMapping: {[key: string]: RedisQueue} = {};

  constructor(
    @extensions() public readonly getJobs: Getter<QueueJob[]>,
    @inject(CoreBindings.APPLICATION_INSTANCE) private application: AuthApplication,
    @inject(QueueBindings.CONFIG) private queueConfig: QueueConfig
  ) {}

  private async initJobs() {
    const jobs = await this.getJobs();
    for (const job of jobs) {
      this.jobMapping[job.name] = job;
    }
  }

  private async initConsumers(queue: RedisQueue, channels: {name: string; concurrency?: number}[]) {
    queue.on('error', function (err) {
      logger.error({err}, `Queue got error of type ${err.message}`);
    });
    queue.on('active', function (job: Job) {
      // jobPromise
      // Job started
      // You can use jobPromise.cancel() to abort this job.
      logger.info(`Job ${job.id} active of type ${job!.data && job.data.routingKey}, ${job!.data && job.data.title}`);
    });
    queue.on('stalled', function (job) {
      // Job that was considered stalled. Useful for debugging job workers that crash or pause the event loop.
      logger.debug(`Job ${job.id} stalled of type ${job!.data && job.data.routingKey}, ${job!.data && job.data.title}`);
    });
    queue.on('progress', function (job, progress) {
      // Job progress updated!
      logger.debug(
        `Job ${job.id} progress of type ${job!.data && job.data.routingKey} with progess ${progress.toString()}`
      );
    });
    queue.on('completed', function (job) {
      // result
      // Job completed with output result!
      logger.info(
        `Job ${job.id} completed of type ${job!.data && job.data.routingKey}, ${job!.data && job.data.title}`
      );
    });
    queue.on('failed', function (job, err) {
      // Job failed with reason err!
      const message = `Job ${job.id} failed of type ${job!.data && job.data.routingKey} with error ${err}`;
      logger.error({err}, message);
    });

    const handlerFn = async (job: Job<any>) => {
      if (!(job?.data && job.data.routingKey)) {
        const err = new Error('Job routingKey missing');
        logger.error({err});
        throw err;
      }
      const key = job.data.routingKey;
      if (!this.jobMapping[key]) {
        const err = new Error(`Job ${key} file is missing`);
        logger.error({err});
        throw err;
      }

      const title = job?.data && job.data.title;
      try {
        logger.info(`[START] Job ${job.id} with title: ${title}`);
        await this.jobMapping[key].handle(job);
        logger.info(`[DONE] Job ${job.id} with title: ${title}`);
      } catch (err) {
        logger.error({err}, `[FAILED] Job ${job.id} with title ${title}`);
        throw err;
      }
    };

    // Set handler in every channels
    for (const channel of channels) {
      if (channel.concurrency) {
        queue.process(channel.name, channel.concurrency, handlerFn);
      } else {
        queue.process(channel.name, handlerFn);
      }
    }
  }

  private async initQueues() {
    const options = {
      redis: {
        port: process.env.REDIS_PORT || 6379, // Redis port
        host: process.env.REDIS_HOST, // Redis host
        family: 4, // 4 (IPv4) or 6 (IPv6)
        password: process.env.REDIS_PASSWORD,
        db: 0
      },
      limiter: {
        max: 100,
        duration: 1000
      }
    } as QueueOptions;
    for (const queueItem of this.queueConfig.queues) {
      const queue = new RedisQueue(queueItem.name, options);
      if (queueItem.channels.length > 0) {
        await this.initConsumers(queue, queueItem.channels);
      }
      this.queueMapping[queueItem.name] = queue;
    }
    this.application.bind(QueueBindings.QUEUE_MAPPING).to(this.queueMapping);
  }

  async start() {
    await this.initQueues();
    await this.initJobs();
    // TODO: Handle retry failed jobs & Pending Jobs
  }

  async stop() {
    for (const queue of Object.values(this.queueMapping)) {
      queue.close();
    }
  }
}
