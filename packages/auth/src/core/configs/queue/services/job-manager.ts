import {BadRequestException, ErrorCode} from '../../../../common/exception';
import {getLoggerFromFilename} from '../../../../common/logging';
import {inject} from '@loopback/context';
import {Job} from 'bull';
import {QueueMsg} from '../common/message';
import {QueueBindings} from '../keys';
import {RedisQueue, QueueConfig} from '../types';

const logger = getLoggerFromFilename(__filename, {tags: ['queue', 'job']});

export class JobManager {
  constructor(
    @inject(QueueBindings.CONFIG) private queueConfig: QueueConfig,
    @inject(QueueBindings.QUEUE_MAPPING) private queueMapping: {[key: string]: RedisQueue},
  ) {}

  async putToQueue(jobName: string, parameters: object): Promise<void> {
    const jobDefinition = this.queueConfig.getJobDefinitionFn(jobName, parameters);
    if (!jobDefinition) {
      throw new BadRequestException(QueueMsg.JOB_NOT_DEFINED(jobName), ErrorCode.RESOURCE_NOT_FOUND);
    }
    const queueName = jobDefinition.queueName;
    const queue: RedisQueue = this.queueMapping[queueName];
    const data: any = {...parameters, title: jobDefinition.title, routingKey: jobDefinition.routingKey};
    const job: Job = await queue.add(jobDefinition.channel, data, jobDefinition.jobOpts);
    logger.info(`Put "${jobName}" to queue successfully with id ${job.id}, data: ${JSON.stringify(data)}`);
  }
}
