import Bull, {JobOptions, Job} from 'bull';

export class RedisQueue extends Bull {}

/**
 * Name of the queue job extension point
 */
export const QUEUE_JOB_HANDLER = 'queue.jobHandler';

/**
 * Options for a queue job.
 */
export type QueueJobOptions = {
  name?: string;
};

/**
 * Queue job with an optional name
 */
export class QueueJob {
  private static count = 0;
  public readonly name: string;

  constructor(options: QueueJobOptions) {
    if (options.name) {
      this.name = options.name;
    } else {
      this.name = `job-${++QueueJob.count}`;
    }
  }

  async handle(job: Job): Promise<void> {
    console.log('The `handle(job)` method should be override');
  }
}

type ChannelSetting = {
  name: string;
  concurrency?: number;
};

export type QueueSetting = {
  name: string;
  channels: ChannelSetting[];
};

export interface QueueConfig {
  queues: QueueSetting[];

  /**
   * Get Job definition by the given jobName and parameters
   */
  getJobDefinitionFn: IGetJobDefinitionFn;
}

export type JobDefinition = {
  title: string;
  jobOpts: JobOptions;
  channel: string;
  queueName: string;
  routingKey: string;
  jobName?: string;
  // unique?: boolean; // NOTE: We stop handle the case
};

export interface IGetJobDefinitionFn {
  (jobName: string, ...parameters: any): JobDefinition | null;
}
