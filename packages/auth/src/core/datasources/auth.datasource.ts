import {
  inject,
  lifeCycleObserver,
  LifeCycleObserver,
  ValueOrPromise,
} from '@loopback/core';
import {AnyObject, juggler} from '@loopback/repository';
import config from './mysql.datasource.config.json';
import Settings from '../../settings';

function updateConfig(dsConfig: AnyObject) {
  if (Settings.NODE_ENV) {
    dsConfig.url = Settings.MYSQL_URL;
    dsConfig.host = Settings.MYSQL_HOST;
    dsConfig.port = Settings.MYSQL_PORT;
    dsConfig.user = Settings.MYSQL_USER;
    dsConfig.password = Settings.MYSQL_PASSWORD;
    dsConfig.database = Settings.MYSQL_DATABASE;
  }
  return dsConfig;
}

@lifeCycleObserver('datasource')
export class BaseMysqlDS extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'auth';

  constructor(
    @inject('datasources.config.auth', {optional: true})
      dsConfig: object = config,
  ) {
    super(updateConfig(dsConfig));
  }

  /**
   * Start the datasource when application is started
   */
  start(): ValueOrPromise<void> {
    // Add your logic here to be invoked when the application is started
  }

  /**
   * Disconnect the datasource when application is stopped. This allows the
   * application to be shut down gracefully.
   */
  stop(): ValueOrPromise<void> {
    return super.disconnect();
  }

  /**
   * To execute rawSQL
   * @param rawSQL: string
   */
  async executeRaw(rawSQL: string): Promise<any | any[]> {
    const connector = this.connector;
    return new Promise((resolve, reject) => {
      if (!connector) {
        return resolve(null);
      }
      // eslint-disable-next-line @typescript-eslint/no-floating-promises
      connector.execute!(rawSQL, [], (err: any, data: any|any[]) => {
        if (err) {
          return reject(err);
        }
        resolve(data);
      })
    });
  }
}
