export namespace DecoratorTags {
  /**
   * Binding tag for use case
   */
  export const USE_CASE = 'useCase';
  /**
   * Binding tag for the use case interface
   */
  export const USE_CASE_INTERFACE = 'useCaseInterface';
}
