import {
  Binding,
  BindingFilter,
  BindingFromClassOptions,
  BindingTemplate,
  bindingTemplateFor,
  ContextTags,
  ContextView,
  createBindingFromClass,
  DecoratorFactory,
  inject,
  InjectionMetadata,
  isDynamicValueProviderClass,
  isProviderClass,
  MetadataInspector,
  transformValueOrPromise
} from '@loopback/context';
import {DecoratorTags} from './tags';
import {UseCaseOrProviderClass} from '../abstracts';

/**
 * Representing an interface for useCases. In TypeScript, the `interface` does
 * not have reflections at runtime. We use a string, a symbol or a Function as
 * the type for the useCase interface.
 */
export type UseCaseInterface = string | symbol | Function;

/**
 * Options to register a useCase binding
 */
export interface UseCaseOptions extends BindingFromClassOptions {
  interface?: UseCaseInterface;
}

/**
 * `@useCase` injects a use case instance that matches the class or interface.
 *
 * @param useCaseInterface - Interface for the use case. It can be in one of the
 * following forms:
 *
 * - A class, such as MyUseCase
 * - A string that identifies the interface, such as `'MyUseCase'`
 * - A symbol that identifies the interface, such as `Symbol('MyUseCase')`
 *
 * If not provided, the value is inferred from the design:type of the parameter
 * or property
 *
 * @param metadata
 * @example
 * ```ts
 *
 * const ctx = new Context();
 * ctx.bind('my-use-case').toClass(MyUseCase);
 *
 * export class MyController {
 *   constructor(@useCase(MyUseCase) private myUseCase: MyUseCase) {}
 * }
 *
 * ctx.bind('my-controller').toClass(MyController);
 * await myController = ctx.get<MyController>('my-controller');
 * ```
 */
export function useCase(useCaseInterface?: UseCaseInterface, metadata?: InjectionMetadata) {
  return inject('', {decorator: '@useCase', ...metadata}, (ctx, injection, session) => {
    let useCaseType = useCaseInterface;
    if (!useCaseType) {
      if (typeof injection.methodDescriptorOrParameterIndex === 'number') {
        useCaseType = MetadataInspector.getDesignTypeForMethod(injection.target, injection.member!)?.parameterTypes[
          injection.methodDescriptorOrParameterIndex
        ];
      } else {
        useCaseType = MetadataInspector.getDesignTypeForProperty(injection.target, injection.member!);
      }
    }
    if (useCaseType === undefined) {
      const targetName = DecoratorFactory.getTargetName(
        injection.target,
        injection.member,
        injection.methodDescriptorOrParameterIndex
      );
      const msg =
        `No design-time type metadata found while inspecting ${targetName}. ` +
        'You can either use `@useCase(UseCaseClass)` or ensure `emitDecoratorMetadata` is enabled in your TypeScript configuration. ' +
        'Run `tsc --showConfig` to print the final TypeScript configuration of your project.';
      throw new Error(msg);
    }

    if (useCaseType === Object || useCaseType === Array) {
      throw new Error('UseCase class cannot be inferred from design type. Use @useCase(UseCaseClass).');
    }
    const view = new ContextView(ctx, filterByUseCaseInterface(useCaseType));
    const result = view.resolve(session);

    const useCaseTypeName =
      typeof useCaseType === 'string'
        ? useCaseType
        : typeof useCaseType === 'symbol'
        ? useCaseType.toString()
        : useCaseType.name;
    return transformValueOrPromise(result, values => {
      if (values.length === 1) return values[0];
      if (values.length >= 1) {
        throw new Error(`More than one bindings found for ${useCaseTypeName}`);
      } else {
        if (metadata?.optional) {
          return undefined;
        }
        throw new Error(
          `No binding found for ${useCaseTypeName}. Make sure a use case ` +
            `binding is created in context ${ctx.name} with useCaseInterface (${useCaseTypeName}).`
        );
      }
    });
  });
}

/**
 * Create a binding filter by use case class
 * @param useCaseInterface - Use case class matching the one used by `binding.toClass()`
 */
export function filterByUseCaseInterface(useCaseInterface: UseCaseInterface): BindingFilter {
  return binding =>
    binding.valueConstructor === useCaseInterface ||
    binding.tagMap[DecoratorTags.USE_CASE_INTERFACE] === useCaseInterface;
}

/**
 * Create a use case binding from a class or provider
 * @param cls - UseCase class or provider
 * @param options - UseCase options
 */
export function createUseCaseBinding<S>(cls: UseCaseOrProviderClass<S>, options: UseCaseOptions = {}): Binding<S> {
  let name = options.name;
  if (!name && isProviderClass(cls)) {
    // Trim `Provider` from the default use case name
    // This is needed to keep backward compatibility
    const templateFn = bindingTemplateFor(cls);
    const template = Binding.bind<S>('template').apply(templateFn);
    if (template.tagMap[ContextTags.PROVIDER] && !template.tagMap[ContextTags.NAME]) {
      // The class is a provider and no `name` tag is found
      name = cls.name.replace(/Provider$/, '');
    }
  }
  if (!name && isDynamicValueProviderClass(cls)) {
    // Trim `Provider` from the default use case name
    const templateFn = bindingTemplateFor(cls);
    const template = Binding.bind<S>('template').apply(templateFn);
    if (template.tagMap[ContextTags.DYNAMIC_VALUE_PROVIDER] && !template.tagMap[ContextTags.NAME]) {
      // The class is a provider and no `name` tag is found
      name = cls.name.replace(/Provider$/, '');
    }
  }
  const binding = createBindingFromClass(cls, {
    name,
    type: DecoratorTags.USE_CASE,
    ...options
  }).apply(asUseCase(options.interface ?? cls));
  return binding;
}

/**
 * Create a binding template for a use case interface
 * @param useCaseInterface - use case interface
 */
export function asUseCase(useCaseInterface: UseCaseInterface): BindingTemplate {
  return function useCaseTemplate(binding: Binding) {
    binding.tag({
      [ContextTags.TYPE]: DecoratorTags.USE_CASE,
      [DecoratorTags.USE_CASE_INTERFACE]: useCaseInterface
    });
  };
}
