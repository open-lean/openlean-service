import {bind, Provider} from '@loopback/core';
import {asMiddleware, Middleware} from '@loopback/express';
import {getLoggerFromFilename} from '../../common/logging';

const logger = getLoggerFromFilename(__filename, {tags: ['middleware', 'error']});

@bind(
  asMiddleware({
    group: 'logger',
    downstreamGroups: [
      'cors',
      'invokeMethod'
    ],
    upstreamGroups: [
      'sendResponse'
    ]
  })
)
export class LogErrorMiddlewareProvider implements Provider<Middleware> {
  constructor() {
  }

  value(): Middleware {
    return async (ctx, next) => {
      const {request, response} = ctx;
      try {
        const result = await next();
        return result;
      } catch (err) {
        if (err.message) {
          const index = err.message.search('HEAD /live');  //prevent add 'not found the endpoint HEAD /live' error to log
          if (index !== -1) return;
        }
        logger.error({err, request, response});
        throw err;
      }
    };
  }
}