import {RoleTypes} from '../../../common/constants';
import {belongsTo, Entity, model, property} from '@loopback/repository';
import {User, UserWithRelations} from './user.model';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthRoleMappings'}
  }
})
export class RoleMapping extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @belongsTo(() => User, {name: 'user'}, {unsigned: true, required: true})
  userId: number;

  @property({
    type: 'string',
    required: true,
    default: RoleTypes.USER,
    length: 20,
    mysql: {
      columnName: 'roleName',
      dataType: 'varchar',
      dataLength: 20,
      default: RoleTypes.USER,
      nullable: 'N'
    }
  })
  roleName: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<RoleMapping>) {
    super(data);
  }
}

export interface RoleMappingRelations {
  // describe navigational properties here
  user: UserWithRelations;
}

export type RoleMappingWithRelations = RoleMapping & RoleMappingRelations;
