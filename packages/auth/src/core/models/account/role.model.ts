import {RoleTypes} from '../../../common/constants';
import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthRoles'}
  }
})
export class Role extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    default: RoleTypes.USER,
    length: 20,
    mysql: {
      columnName: 'name',
      dataType: 'varchar',
      dataLength: 20,
      default: RoleTypes.USER,
      nullable: 'N'
    }
  })
  name: string;

  @property({
    type: 'string',
    length: 256,
    mysql: {
      columnName: 'description',
      dataType: 'varchar',
      dataLength: 256,
      nullable: 'Y'
    }
  })
  description?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<Role>) {
    super(data);
  }
}

export interface RoleRelations {
  // describe navigational properties here
}

export type RoleWithRelations = Role & RoleRelations;
