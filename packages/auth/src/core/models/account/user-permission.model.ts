import {belongsTo, model, property} from '@loopback/repository';
import {AuthorEntity} from '../base-entity';
import {TimestampMixin} from '../mixins';
import {User, UserWithRelations} from './user.model';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthUserPermissions'},
    indexes: {
      uqUserPermission: {
        keys: {
          userId: 1,
          modelId: 1,
          modelType: 1,
          scope: 1
        },
        options: {
          unique: true
        }
      }
    }
  }
})
export class UserPermission extends TimestampMixin(AuthorEntity) {
  // Define well-known properties here
  @property({
    type: 'number',
    id: true,
    generated: true,
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    required: true,
    mysql: {
      columnName: 'modelId',
      nullable: 'N'
    }
  })
  modelId: number;

  @property({
    type: 'string',
    length: 45,
    required: true,
    mysql: {
      columnName: 'modelType',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'N'
    }
  })
  modelType: string;

  @property({
    type: 'string',
    required: true,
    length: 10,
    jsonSchema: {
      enum: ['user', 'admin']
    },
    default: 'user',
    mysql: {
      columnName: 'target',
      dataType: 'varchar',
      dataLength: 10,
      default: 'user',
      nullable: 'N'
    }
  })
  target: string;

  @property({
    type: 'string',
    length: 50,
    required: true,
    mysql: {
      columnName: 'scope',
      dataType: 'varchar',
      dataLength: 50,
      nullable: 'N'
    }
  })
  scope: string;

  @belongsTo(() => User)
  userId: number;

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<UserPermission>) {
    super(data);
  }
}

export interface UserPermissionRelations {
  // describe navigational properties here
  users?: UserWithRelations;
}

export type UserPermissionWithRelations = UserPermission & UserPermissionRelations;
