import {TimestampMixin} from '../mixins';
import {hasMany, model, property, Entity} from '@loopback/repository';
import {RoleMapping, RoleMappingWithRelations} from './role-mapping.model';
import {UserPermission, UserPermissionWithRelations} from './user-permission.model';

@model({
  settings: {
    strict: true,
    forceId: false,
    mysql: {schema: 'openlean', table: 'AuthUsers'},
    hiddenProperties: ['password', 'verificationToken', 'emailVerified']
  }
})
export class User extends TimestampMixin(Entity) {
  @property({
    type: 'number',
    id: true,
    generated: true,
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 45,
    index: {unique: true},
    mysql: {
      columnName: 'username',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'N'
    }
  })
  username: string;

  @property({
    type: 'string',
    required: true,
    length: 45,
    index: {unique: true},
    mysql: {
      columnName: 'email',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'N'
    }
  })
  email: string;

  @property({
    type: 'string',
    length: 45,
    mysql: {
      columnName: 'firstName',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'Y'
    }
  })
  firstName?: string;

  @property({
    type: 'string',
    length: 45,
    mysql: {
      columnName: 'lastName',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'Y'
    }
  })
  lastName?: string;

  @property({
    type: 'string',
    required: true,
    length: 60,
    mysql: {
      columnName: 'password',
      dataType: 'varchar',
      dataLength: 60,
      nullable: 'N'
    }
  })
  password: string;

  @property({
    type: 'string',
    length: 256,
    required: false,
    mysql: {
      columnName: 'verificationToken',
      dataType: 'varchar',
      dataLength: 256,
      nullable: 'Y'
    }
  })
  verificationToken?: string;

  @property({
    type: 'boolean',
    required: true,
    default: false,
    mysql: {
      columnName: 'emailVerified',
      dataType: 'tinyint',
      dataPrecision: 3,
      dataScale: 0,
      default: 0,
      nullable: 'N'
    }
  })
  emailVerified: boolean;

  @property({
    type: 'boolean',
    required: true,
    default: false,
    mysql: {
      columnName: 'disabled',
      dataType: 'tinyint',
      dataPrecision: 3,
      dataScale: 0,
      default: 0,
      nullable: 'N'
    }
  })
  disabled: boolean;

  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'createdBy',
      nullable: 'Y'
    }
  })
  createdBy?: number;

  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'updatedBy',
      nullable: 'Y'
    }
  })
  updatedBy?: number;

  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'companyId',
      nullable: 'N'
    }
  })
  companyId?: number;

  @hasMany(() => RoleMapping, {name: 'roleMapping'})
  roleMappings?: RoleMapping;

  @hasMany(() => UserPermission)
  permissions?: UserPermission[];

  // Define well-known properties here

  // Indexer property to allow additional data
  [prop: string]: any;

  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
  roleMapping: RoleMappingWithRelations;
  permissions?: UserPermissionWithRelations[];
}

export type UserWithRelations = User & UserRelations;
