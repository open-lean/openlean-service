import {belongsTo, model, property, Entity} from '@loopback/repository';
import {User, UserWithRelations} from '../account';
import {SystemConstant, AccessTokenMsg} from '../../../common/constants';
import {TimestampMixin} from '../mixins';
import {UnprocessableEntityException} from '../../../common/exception';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthToken'}
  }
})
export class AuthToken extends TimestampMixin(Entity) {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
    mysql: {
      columnName: 'id',
      dataType: 'varchar',
      dataLength: 256,
      nullable: 'N'
    }
  })
  id: string;

  @belongsTo(() => User, {name: 'user'}, {unsigned: true, mysql: {nullable: 'N'}})
  userId: number;

  @property({
    type: 'number',
    unsigned: true
  })
  companyId?: number;

  @property({
    type: 'number',
    unsigned: true
  })
  deviceId?: number;

  @property({
    type: 'number',
    required: true,
    mysql: {
      dataType: 'int',
      nullable: 'N'
    }
  })
  ttl: number;

  @property({
    type: 'date',
    required: true,
    mysql: {
      columnName: 'issuedAt',
      dataType: 'timestamp',
      default: 'CURRENT_TIMESTAMP',
      nullable: 'N'
    }
  })
  issuedAt: Date;

  @property({
    type: 'date',
    required: false,
    mysql: {
      columnName: 'expiredAt',
      dataType: 'timestamp',
      nullable: 'Y'
    }
  })
  expiredAt?: Date;

  @property({
    type: 'string',
    index: {unique: true},
    mysql: {
      dataType: 'varchar',
      dataLength: 256
    }
  })
  refreshToken?: string;

  @property({
    type: 'string',
    required: false,
    length: 10,
    jsonSchema: {
      enum: ['Bearer', 'MAC']
    },
    mysql: {
      columnName: 'tokenType',
      dataType: 'varchar',
      dataLength: 10,
      nullable: 'Y'
    }
  })
  tokenType?: string;

  @property({
    type: 'string',
    mysql: {
      dataType: 'varchar',
      dataLength: 256
    }
  })
  scopes?: string;

  @property({
    type: 'number',
    required: true,
    default: SystemConstant.SERVICE_TYPE,
    mysql: {
      columnName: 'serviceType',
      dataType: 'tinyint',
      nullable: 'N'
    }
  })
  serviceType: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<AuthToken>) {
    super(data);
  }

  async validate(): Promise<boolean> {
    if (this.ttl === 0) {
      throw new UnprocessableEntityException(AccessTokenMsg.TTL_NOT_BE_ZERO);
    }
    if (this.ttl < -1) {
      throw new UnprocessableEntityException(AccessTokenMsg.TTL_GREATER_MINUS_ONE);
    }
    const now = Date.now();
    const updated = this.updatedOn.getTime();
    const elapsedSeconds = (now - updated) / 1000;
    return this.ttl === -1 || this.ttl > elapsedSeconds;
  }
}

export interface AuthTokenRelations {
  // describe navigational properties here
  user: UserWithRelations;
}

export type AuthTokenWithRelations = AuthToken & AuthTokenRelations;
