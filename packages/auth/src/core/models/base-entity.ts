import {Entity, model, property} from '@loopback/repository';

@model()
export class AuthorEntity extends Entity {
  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'createdBy',
      nullable: 'Y'
    }
  })
  createdBy?: number;

  @property({
    type: 'number',
    unsigned: true,
    dataType: 'int',
    mysql: {
      columnName: 'updatedBy',
      nullable: 'Y'
    }
  })
  updatedBy?: number;

  constructor(data?: Partial<AuthorEntity>) {
    super(data);
  }
}