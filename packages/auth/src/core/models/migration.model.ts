import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthMigrations'}
  }
})
export class Migration extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 100,
    mysql: {
      columnName: 'name',
      dataType: 'varchar',
      dataLength: 100,
      nullable: 'N'
    }
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    length: 20,
    mysql: {
      columnName: 'version',
      dataType: 'varchar',
      dataLength: 20,
      nullable: 'N'
    }
  })
  version: string;

  @property({
    type: 'date',
    required: true,
    default: '$now',
    mysql: {
      columnName: 'createdOn',
      dataType: 'timestamp',
      default: 'CURRENT_TIMESTAMP',
      nullable: 'N'
    }
  })
  createdOn: Date;

  @property({
    type: 'number',
    required: true,
    mysql: {
      columnName: 'serviceType',
      dataType: 'tinyint',
      nullable: 'N'
    }
  })
  serviceType: number;

  // Define well-known properties here

  // Indexer property to allow additional data

  [prop: string]: any;

  constructor(data?: Partial<Migration>) {
    super(data);
  }
}

export interface MigrationRelations {
  // describe navigational properties here
}

export type MigrationWithRelations = Migration & MigrationRelations;
