import {MixinTarget} from '@loopback/core';
import {Model, property} from '@loopback/repository';

export function AuthorMixin<T extends MixinTarget<Model>> (
  superClass: T
) {
  class MixedModel extends superClass {
    @property({
      type: 'number',
      unsigned: true,
      dataType: 'int',
      mysql: {
        columnName: 'createdBy',
        nullable: 'Y'
      }
    })
    createdBy?: number;

    @property({
      type: 'number',
      unsigned: true,
      dataType: 'int',
      mysql: {
        columnName: 'updatedBy',
        nullable: 'Y'
      }
    })
    updatedBy?: number;

    constructor(...args: any[]) {
      super(args[0]);
    }
  }

  return MixedModel;
}