import {MixinTarget} from '@loopback/core';
import {property, Model} from '@loopback/repository';

/**
 * A mixin factory to add `createdOn, updatedOn` property
 *
 * @param superClass - Base Class
 * @typeParam T - Model class
 */
export function TimestampMixin<T extends MixinTarget<Model>>(
  superClass: T
) {
  class MixedModel extends superClass {
    @property({
      type: 'date',
      required: true,
      default: '$now',
      mysql: {
        columnName: 'createdOn',
        dataType: 'timestamp',
        default: 'CURRENT_TIMESTAMP',
        nullable: 'N'
      }
    })
    createdOn: Date;

    @property({
      type: 'date',
      required: true,
      default: '$now',
      mysql: {
        columnName: 'updatedOn',
        dataType: 'timestamp',
        default: 'CURRENT_TIMESTAMP',
        nullable: 'N'
      }
    })
    updatedOn: Date;

    constructor(...args: any[]) {
      super(args[0]);
    }
  }

  return MixedModel;
}