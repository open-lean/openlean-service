import {model, property, Entity} from '@loopback/repository';
import {JobStatus} from '../configs/queue';
import {TimestampMixin} from './mixins';
import {SystemConstant} from '../../common/constants';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'ktpulse', table: 'QueueJob'}
  }
})
export class QueueJob extends TimestampMixin(Entity) {
  @property({
    type: 'number',
    id: true,
    generated: true,
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @property({
    type: 'string',
    required: false,
    mysql: {
      columnName: 'jobId',
      dataType: 'varchar',
      dataLength: 256,
      nullable: 'Y'
    }
  })
  jobId?: string;

  @property({
    type: 'string',
    required: true,
    index: true,
    mysql: {
      columnName: 'title',
      dataType: 'varchar',
      dataLength: 100,
      nullable: 'N'
    }
  })
  title: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      columnName: 'name',
      dataType: 'varchar',
      dataLength: 100,
      nullable: 'N'
    }
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    default: JobStatus.PENDING,
    mysql: {
      columnName: 'status',
      dataType: 'varchar',
      dataLength: 25,
      default: JobStatus.PENDING,
      nullable: 'N'
    }
  })
  status: string;

  @property({
    type: 'string',
    required: true,
    mysql: {
      columnName: 'data',
      dataType: 'text',
      nullable: 'N'
    }
  })
  data: string;

  @property({
    type: 'string',
    required: false,
    mysql: {
      columnName: 'reason',
      dataType: 'varchar',
      dataLength: 512,
      nullable: 'Y'
    }
  })
  reason?: string;

  @property({
    type: 'number',
    required: true,
    default: SystemConstant.SERVICE_TYPE,
    mysql: {
      columnName: 'fromService',
      dataType: 'tinyint',
      default: SystemConstant.SERVICE_TYPE,
      nullable: 'N'
    }
  })
  fromService: number;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<QueueJob>) {
    super(data);
  }
}

export interface QueueJobRelations {
  // describe navigational properties here
}

export type QueueJobWithRelations = QueueJob & QueueJobRelations;
