import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    strict: true,
    mysql: {schema: 'openlean', table: 'AuthSecretKeys'}
  }
})
export class SecretKey extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
    mysql: {
      columnName: 'id',
      nullable: 'N'
    }
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
    length: 45,
    mysql: {
      columnName: 'key',
      dataType: 'varchar',
      dataLength: 45,
      nullable: 'N'
    }
  })
  key: string;

  @property({
    type: 'date',
    required: true,
    mysql: {
      columnName: 'expired_on',
      dataType: 'datetime',
      nullable: 'N'
    }
  })
  expiredOn: Date;

  @property({
    type: 'date',
    required: true,
    default: '$now',
    mysql: {
      columnName: 'createdOn',
      dataType: 'timestamp',
      default: 'CURRENT_TIMESTAMP',
      nullable: 'N'
    }
  })
  createdOn: Date;

  // Define well-known properties here

  // Indexer property to allow additional data

  [prop: string]: any;

  constructor(data?: Partial<SecretKey>) {
    super(data);
  }
}

export interface SecretKeyRelations {
  // describe navigational properties here
}

export type SecretKeyWithRelations = SecretKey & SecretKeyRelations;
