import {DefaultCrudRepository, Entity, IsolationLevel, Transaction, Where} from '@loopback/repository';
import {Count, DataObject, Options} from '@loopback/repository/src/common-types';
import {BadRequestException, UnprocessableEntityException} from '../../../common/exception';
import {juggler} from '@loopback/repository/src/repositories/legacy-juggler-bridge';
import { convertDateToString, formatTextWithData } from '../../../common/utils';
import _ from 'lodash';

export class EntityNotFoundError<ID, Props extends object = {}> extends Error {
  code: string;
  entityName: string;
  entityId: ID;

  constructor(
    entityOrName: typeof Entity | string,
    entityId: ID,
    extraProperties?: Props
  ) {
    const entityName =
      typeof entityOrName === 'string'
        ? entityOrName
        : entityOrName.modelName || entityOrName.name;

    const quotedId = JSON.stringify(entityId);

    super(`Entity not found: ${entityName} with id ${quotedId}`);

    Error.captureStackTrace(this, this.constructor);

    this.code = 'ENTITY_NOT_FOUND';
    this.entityName = entityName;
    this.entityId = entityId;

    Object.assign(this, extraProperties);
  }
}

export class BaseRepository<E extends Entity,
  IdType,
  Relations extends object> extends DefaultCrudRepository<E, IdType, Relations> {

  constructor(
    public entityClass: typeof Entity & {prototype: E},
    public dataSource: juggler.DataSource
  ) {
    super(entityClass, dataSource);
  }

  async getTransaction(isolationLevel?: IsolationLevel, timeout?: number): Promise<Transaction> {
    if (!isolationLevel) {
      isolationLevel = IsolationLevel.READ_COMMITTED;
    }
    const options: any = {isolationLevel};
    if (timeout) {
      options.timeout = timeout;
    }
    const tx = (await this.dataSource.beginTransaction(options)) as Transaction;
    return tx;
  }

  getAllProperties(): string[] {
    const output = Object.keys(this.entityClass.definition.properties);
    return output;
  }

  /**
   * Get the first Id field in the entity
   */
  getIdField(): string {
    let idField = 'id';
    const idFields = this.entityClass.getIdProperties();
    if (idFields.length > 0) {
      idField = idFields[0];
    }
    return idField;
  }

  async bulkUpdate(entities: DataObject<E>[], updateFields: string[], batchSize = 1000): Promise<number> {
    // eslint-disable-next-line no-prototype-builtins
    if (batchSize < 1) {
      throw new BadRequestException('Batch size must be a positive integer.');
    }
    if (updateFields.length === 0) {
      throw new BadRequestException('updateFields names must be given to bulk_update().');
    }
    const idField = this.getIdField();
    const caseClauseTemplate = '{column} = (CASE {idColumn} {when}';
    const tailEndTemplate = 'END)';
    let idLength = 0;
    for (let from = 0, length = entities.length; from < length; from = from + batchSize) {
      const max = Math.min(from + batchSize, length);
      const ids = [];
      const caseClauses: any = {};
      let index = from;
      while (index < max) {
        const entity: any = entities[index];
        ids.push(entity[idField]);
        for (const field of updateFields) {
          if (!caseClauses[field]) {
            caseClauses[field] = formatTextWithData(caseClauseTemplate, {column: field, idColumn: idField});
          }
          let fieldData = _.isDate(entity[field]) ? convertDateToString(entity[field]) : entity[field];
          fieldData = _.isObject(fieldData) ? JSON.stringify(fieldData) : fieldData;
          fieldData = _.isString(fieldData) ? JSON.stringify(fieldData) : fieldData;
          caseClauses[field] = formatTextWithData(caseClauses[field],
            {when: `WHEN ${entity[idField]} THEN ${fieldData} {when}`});
        }
        index++;
      }
      if (ids.length > 0) {
        let values: string[] = Object.values(caseClauses);
        values = values.map(caseClause => formatTextWithData(caseClause, {when: tailEndTemplate}));
        const valuesByStr = values.join(', ');
        const inClauseSQL = `(${ids.join(', ')})`;
        const sql = `UPDATE ${this.entityClass.modelName} SET ${valuesByStr} WHERE ${idField} IN ${inClauseSQL}`;
        await (this.dataSource as any).executeRaw(sql);
        idLength += ids.length;
      }
    }
    return idLength;
  }

  async updateAttributesWithWhere(
    data: DataObject<E>,
    where?: Where<E>,
    options?: Options
  ): Promise<Count> {
    where = where ?? {};
    const result: any = await this.modelClass.updateAll(where, data, options);
    return {count: result.count};
  }

  async updateAttributesById(
    id: IdType,
    data: DataObject<E>,
    options?: Options
  ): Promise<void> {
    if (id === undefined) {
      throw new UnprocessableEntityException('Invalid Argument: id cannot be undefined');
    }
    const where = {id} as Where<E>;
    const result = await this.updateAttributesWithWhere(data, where, options);
    if (result.count === 0) {
      throw new EntityNotFoundError(this.entityClass, id);
    }
  }
}