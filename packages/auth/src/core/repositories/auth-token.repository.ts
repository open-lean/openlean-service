import {BelongsToAccessor, Getter, repository} from '@loopback/repository';
import {BaseRepository} from './abstracts';
import {AuthToken, AuthTokenRelations, User} from '../models';
import {BaseMysqlDS} from '../datasources';
import {inject} from '@loopback/core';
import {UserRepository} from './user.repository';

export class AuthTokenRepository extends BaseRepository<
  AuthToken,
  typeof AuthToken.prototype.id,
  AuthTokenRelations
> {
  public readonly user: BelongsToAccessor<User, typeof AuthToken.prototype.id>;

  constructor(
    @inject('datasources.auth') dataSource: BaseMysqlDS,
    @repository.getter('UserRepository')
      userRepositoryGetter: Getter<UserRepository>,
  ) {
    super(AuthToken, dataSource);
    this.user = this.createBelongsToAccessorFor('user', userRepositoryGetter);

    // add this line to register inclusion resolver
    this.registerInclusionResolver('user', this.user.inclusionResolver);
  }
}
