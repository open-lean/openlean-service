import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {BaseMysqlDS} from '../datasources';
import {Migration, MigrationRelations} from '../models';

export class MigrationRepository extends DefaultCrudRepository<
  Migration,
  typeof Migration.prototype.name,
  MigrationRelations
> {
  constructor(@inject('datasources.auth') dataSource: BaseMysqlDS) {
    super(Migration, dataSource);
  }
}
