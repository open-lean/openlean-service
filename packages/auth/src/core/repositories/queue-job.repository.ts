import {inject} from '@loopback/core';
import {BaseRepository} from './abstracts';
import {BaseMysqlDS} from '../datasources';
import {QueueJob, QueueJobRelations} from '../models';

export class QueueJobRepository extends BaseRepository<
  QueueJob,
  typeof QueueJob.prototype.id,
  QueueJobRelations
> {
  constructor(
    @inject('datasources.auth') dataSource: BaseMysqlDS,
  ) {
    super(QueueJob, dataSource);
  }
}
