import {inject} from '@loopback/core';
import {BaseRepository} from './abstracts';
import {BaseMysqlDS} from '../datasources';
import {RoleMapping, RoleMappingRelations} from '../models';

export class RoleMappingRepository extends BaseRepository<
  RoleMapping,
  typeof RoleMapping.prototype.id,
  RoleMappingRelations
> {
  constructor(@inject('datasources.auth') dataSource: BaseMysqlDS) {
    super(RoleMapping, dataSource);
  }
}
