import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {BaseMysqlDS} from '../datasources';
import {Role, RoleRelations} from '../models';

export class RoleRepository extends DefaultCrudRepository<Role, typeof Role.prototype.name, RoleRelations> {
  constructor(@inject('datasources.auth') dataSource: BaseMysqlDS) {
    super(Role, dataSource);
  }
}
