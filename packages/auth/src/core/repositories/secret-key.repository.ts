import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {BaseMysqlDS} from '../datasources';
import {SecretKey, SecretKeyRelations} from '../models';

export class SecretKeyRepository extends DefaultCrudRepository<
  SecretKey,
  typeof SecretKey.prototype.name,
  SecretKeyRelations
> {
  constructor(@inject('datasources.auth') dataSource: BaseMysqlDS) {
    super(SecretKey, dataSource);
  }
}
