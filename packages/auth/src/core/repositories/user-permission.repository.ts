import {BaseRepository} from './abstracts';
import {UserPermission, UserPermissionRelations} from '../models';
import {inject} from '@loopback/core';
import {BaseMysqlDS} from '../datasources';

export class UserPermissionRepository extends BaseRepository<
  UserPermission,
  typeof UserPermission.prototype.id,
  UserPermissionRelations
> {
  constructor(@inject('datasources.auth') dataSource: BaseMysqlDS) {
    super(UserPermission, dataSource);
  }
}
