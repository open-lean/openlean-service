import {BaseRepository} from './abstracts';
import {inject} from '@loopback/core';
import {Getter, HasManyRepositoryFactory, repository, HasOneRepositoryFactory} from '@loopback/repository';
import {BaseMysqlDS} from '../datasources';
import {RoleMapping, User, UserPermission, UserRelations} from '../models';
import {RoleMappingRepository} from './role-mapping.repository';
import {UserPermissionRepository} from './user-permission.repository';

export class UserRepository extends BaseRepository<User, typeof User.prototype.id, UserRelations> {
  public roleMappings: HasManyRepositoryFactory<RoleMapping, typeof User.prototype.id>;

  public readonly permissions: HasManyRepositoryFactory<UserPermission, typeof User.prototype.id>;

  constructor(
    @inject('datasources.auth') dataSource: BaseMysqlDS,
    @repository(RoleMappingRepository) roleMappingRepository: RoleMappingRepository,
    @repository.getter('UserPermissionRepository') userPermissionRepositoryGetter: Getter<UserPermissionRepository>,
  ) {
    super(User, dataSource);
    this.permissions = this.createHasManyRepositoryFactoryFor('permissions', userPermissionRepositoryGetter);
    this.roleMappings = this.createHasManyRepositoryFactoryFor('roleMapping', async () => roleMappingRepository);

    // add this line to register inclusion resolver
    this.registerInclusionResolver('roleMapping', this.roleMappings.inclusionResolver);
    this.registerInclusionResolver('permissions', this.permissions.inclusionResolver);
  }

  /**
   * To get user role by the given user ID
   * @param [userId] The ID of user
   */

  async getRoles(
    userId: typeof User.prototype.id
  ): Promise<string[]> {
    const roleMappings = await this.roleMappings(userId).find();
    return roleMappings.map(item => item.roleName);
  }

  /**
   * To get list of permissions by the given userID
   * @param [userId] The ID of user
   */
  async getPermissions(userId: typeof User.prototype.id): Promise<UserPermission[]> {
    return this.permissions(userId).find();
  }
}
