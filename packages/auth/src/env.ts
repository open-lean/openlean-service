import * as dotenv from 'dotenv';
import * as path from 'path';
import {loadSettings} from './settings';

function loadEnv() {
  let envFile = '../.env';
  switch (process.env.NODE_ENV) {
    case 'production':
      envFile = '../.env.production';
      break;
    case 'staging':
      envFile = '../.env.staging';
      break;
  }
  dotenv.config({path: path.join(__dirname, envFile)});
  loadSettings();
}
loadEnv();
