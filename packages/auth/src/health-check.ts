import {inject, Provider} from '@loopback/context';
import {Application} from '@loopback/core';
import {HealthTags, LiveCheck, ReadyCheck} from '@loopback/extension-health';
import {BaseMysqlDS, RedisDataSource} from './core/datasources';

const myLiveCheck: LiveCheck = () => {
  return Promise.resolve();
};

// Define a provider to check the liveliness of a datasource
class MySQLLiveCheckProvider implements Provider<LiveCheck> {
  constructor(@inject('datasources.auth') private ds: BaseMysqlDS) {}

  value() {
    return () => this.ds.ping();
  }
}

const myReadyCheck: ReadyCheck = () => {
  return Promise.resolve();
};

// Define a provider to check the liveness of a datasource
class RedisLiveCheckProvider implements Provider<LiveCheck> {
  constructor(@inject('datasources.redis') private ds: RedisDataSource) {}

  value() {
    return () => this.ds.ping();
  }
}

export default function addingLiveAndReadyCheck(app: Application) {
  app.bind('health.MyLiveCheck').to(myLiveCheck).tag(HealthTags.LIVE_CHECK);
  app.bind('health.MySQLCheck').toProvider(MySQLLiveCheckProvider).tag(HealthTags.LIVE_CHECK);
  app.bind('health.MyReadyCheck').to(myReadyCheck).tag(HealthTags.READY_CHECK);
  app
    .bind('health.RedisCheck')
    .toProvider(RedisLiveCheckProvider)
    .tag(HealthTags.LIVE_CHECK);
}
