import './env';

import path from 'path';
import fs from 'fs';
import {AuthApplication} from './application';
import {MigrationRepository} from './core/repositories';
import {SystemConstant} from './common/constants';
import {Migration} from './core/models';


export async function importData(args: string[]) {
  console.log('Migrating data...');
  const migrationsPath = path.join(__dirname, '/migrations');
  const files = fs.readdirSync(migrationsPath);

  const app = new AuthApplication();
  await app.boot();
  const migrationRepository = await app.getRepository(MigrationRepository);
  const migrations = await migrationRepository.find({
    where: {
      serviceType: SystemConstant.SERVICE_TYPE
    },
    fields: {'name': true, 'version': true}
  });
  const importedList: any = {};
  migrations.forEach((item) => {
    importedList[item.name] = item.version;
  });

  for (const file of files) {
    const fileName = file.split('.')[0];
    if (!importedList[fileName]) {
      console.log('Applying migration file:', fileName);
      const migration = require('./migrations/' + fileName).default;
      await migration.run(app);
      const data = {
        name: fileName,
        version: migration.version,
        serviceType: SystemConstant.SERVICE_TYPE
      } as Migration;
      await migrationRepository.create(data);
      importedList[fileName] = migration.version;
      console.log('Applied migration file:', fileName);
    }
  }
  console.log('Imported!!!');
  process.exit(0);
}

importData(process.argv).catch(err => {
  console.error('Cannot importing data', err);
  process.exit(1);
});