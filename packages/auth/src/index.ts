import './env';
import Settings from './settings';

import {ApplicationConfig, AuthApplication} from './application';

export * from './application';

function getConfiguration(): ApplicationConfig {
  const config: any = {
    rest: {
      port: +(Settings.PORT),
      host: Settings.HOST,
      // The `gracePeriodForClose` provides a graceful close for http/https
      // servers with keep-alive clients. The default value is `Infinity`
      // (don't force-close). If you want to immediately destroy all sockets
      // upon stop, set its value to `0`.
      // See https://www.npmjs.com/package/stoppable
      gracePeriodForClose: 10000, // 10 seconds
      openApiSpec: {
        disabled: true
      }
    }
  };
  switch (process.env.NODE_ENV) {
    case 'production':
      config.rest.cors = {
        origin: Settings.CORS_ORIGIN,
        methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
        optionsSuccessStatus: 204,
        maxAge: 86400,
        credentials: true
      };
      config.rest.apiExplorer = {
        disabled: true
      };
      break;
    case 'staging':
      config.rest.openApiSpec = {
        // useful when used with OpenAPI-to-GraphQL to locate your application
        setServersFromRequest: true
      };
      break;
  }
  return config;
}

export async function main() {
  const config = getConfiguration();
  const app = new AuthApplication(config);
  await app.boot();
  await app.start();

  const url = app.restServer.url;
  console.log(`Server is running at ${url}`);

  return app;
}

if (require.main === module) {
  // Run the application
  main().catch(err => {
    console.error('Cannot start the application.', err);
    process.exit(1);
  });
}
