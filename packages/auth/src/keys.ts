export namespace ServiceBindings {
  export const AUTH_TOKEN_SERVICE = 'security.authtoken.service';
  export const AUTHORIZATION_SERVICE = 'security.authorization.service';
  export const JWT_SERVICE = 'security.jwt.service';
  export const ACCOUNT_SERVICE = 'security.account.service';
  export const JOB_MANAGER = 'queue.mapping';
}

export namespace KeyPrefix {
  export const REPOSITORY = 'repositories.';
  export const SERVICE = 'services.'
}