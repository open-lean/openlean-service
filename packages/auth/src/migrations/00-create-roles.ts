import {AuthApplication} from '../application';
import {RoleRepository} from '../core/repositories';
import {RoleTypes} from '../common/constants';
import {Role} from '../core/models';

const migration: any = {
  version: 'v1.0.0'
};

migration.run = async function(app: AuthApplication) {
  const roleRepository = await app.getRepository(RoleRepository);

  await roleRepository.deleteAll();
  const roles = [{
    name: RoleTypes.KT_ADMIN,
    description: 'Users with this role will have full access to our system'
  }, {
    name: RoleTypes.KT_MANAGER,
    description: 'Users with this role will have specific access provided by KTAdmin'
  }, {
    name: RoleTypes.USER,
    description: 'User with this role will be our customer'
  }, {
    name: RoleTypes.USER_APP,
    description: 'Users with this role is similar to the User role. In additional, They can access to our App'
  }, {
      name: RoleTypes.KT_ADMIN_READ,
      description: 'Users with this role will have full read access to our system'
  }] as Role[];
  await roleRepository.createAll(roles);
};

export default migration;