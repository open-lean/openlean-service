import {AuthApplication} from '../application';
import {RoleTypes} from '../common/constants';
import {User} from '../core/models';
import {ServiceBindings} from '../keys';
import {AuthService} from '../modules/auth';

const migration: any = {
  version: 'v1.0.0'
};

migration.run = async function(app: AuthApplication) {
  const accountService = await app.get<AuthService>(`services.${ServiceBindings.ACCOUNT_SERVICE}`);

  const admins = [{
    username: 'ktAdmin2020',
    email: 'ktadmin@leansoft-inc.com',
    firstName: 'leansoft',
    lastName: 'admin',
    password: 'admin2020',
    emailVerified: true,
  }] as User[];

  for (const admin of admins) {
    await accountService.createAdmin(admin, RoleTypes.KT_ADMIN);
  }
};

export default migration;