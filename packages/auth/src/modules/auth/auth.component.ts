import {BaseApplication} from '../../core/abstracts';
import {Component, ContextTags, CoreBindings, inject, injectable, createBindingFromClass} from '@loopback/core';
import {AuthController} from './controllers';
import {AuthBinding} from './keys';
import {AuthService} from './services';
import {JWTAuthenticationStrategy} from './strategies';
import {AuthTokenHandler} from './usecases/token/auth-token.handler';
import {RevokeTokenHandler} from './usecases/revoke/revoke-token.handler';

/**
 * OAuth2 component
 */
@injectable({tags: {[ContextTags.KEY]: AuthBinding.COMPONENT}})
export class AuthComponent implements Component {
  controllers = [AuthController];
  services = [];
  useCases = [AuthTokenHandler, RevokeTokenHandler];

  constructor(
    @inject(CoreBindings.APPLICATION_INSTANCE) private application: BaseApplication,
  ) {
    this.application.add(createBindingFromClass(JWTAuthenticationStrategy));
  }
}
