import {Provider} from '@loopback/context';
import {AuthorizationContext, AuthorizationDecision, AuthorizationMetadata, Authorizer} from '@loopback/authorization';
import _ from 'lodash';
import {inject, Setter} from '@loopback/core';
import {SecurityBindings, UserProfile} from '@loopback/security';
import {AuthUserProfile} from '../types';

export class AuthorizationProvider implements Provider<Authorizer> {
  constructor(
    @inject.setter(SecurityBindings.USER)
    readonly setCurrentUser: Setter<UserProfile>,
  ) {}

  /**
   * @returns authenticateFn
   */
  value(): Authorizer {
    return this.authorize.bind(this);
  }

  async authorize(
    authorizationCtx: AuthorizationContext,
    metadata: AuthorizationMetadata,
  ) {
    let result = AuthorizationDecision.DENY;
    let user = null;
    if (authorizationCtx.principals.length > 0) {
      user = authorizationCtx.principals[0] as AuthUserProfile;
    }

    if (user?.id) {
      const allowedPerms = metadata.allowedRoles;

      const matches = _.intersection(user.permissions, allowedPerms);
      if (matches.length > 0) {
        result = AuthorizationDecision.ALLOW;
        this.setCurrentUser(user);
      }
    }
    return result;
  }
}