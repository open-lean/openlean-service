import {AuthorizationContext, AuthorizationDecision, AuthorizationMetadata} from '@loopback/authorization';
import {TokenServiceBindings} from '../keys';
import {AuthToken} from '../../../core/models';

const DEFAULT_SCOPES = ['DEFAULT'];

/**
 * Check if the scope required by the remote method is allowed
 * by the scopes granted to the requesting access token.
 * @param authorizationCtx
 * @param metadata
 */
export async function scopeAuthorization(
  authorizationCtx: AuthorizationContext,
  metadata: AuthorizationMetadata,
): Promise<AuthorizationDecision> {
  let result = AuthorizationDecision.DENY;

  // For backwards compatibility, methods with no scopes defined
  // are assigned a single "DEFAULT" scope
  const resourceScopes = metadata.scopes ?? DEFAULT_SCOPES;
  let tokenScopes: string[] = DEFAULT_SCOPES;

  // retrieve it from authentication module
  const accessToken = await authorizationCtx.invocationContext.get<AuthToken>(TokenServiceBindings.CURRENT_ACCESS_TOKEN, {
    optional: true,
  });

  if (accessToken?.scopes) {
    tokenScopes = accessToken.scopes.split(', ');
  }

  // Scope is allowed when at least one of token's scopes
  // is found in method's (resource's) scopes.
  const isScopeAllowed = Array.isArray(tokenScopes) && Array.isArray(resourceScopes) &&
    resourceScopes.some(s => tokenScopes.indexOf(s) !== -1);

  if (isScopeAllowed) {
    result = AuthorizationDecision.ALLOW;
  }
  return result;
}