import {AuthErrorCode} from './constants';

export class AuthError extends Error {
  code: string;
  uri?: string;
  status: number;
  name = 'AuthError';

  static readonly CODE_MAPPING: {[code: string]: number} = {
    [AuthErrorCode.INVALID_REQUEST]: 400,
    [AuthErrorCode.INVALID_CLIENT]: 401,
    [AuthErrorCode.INVALID_GRANT]: 403,
    [AuthErrorCode.INVALID_SCOPE]: 400,
    [AuthErrorCode.ACCESS_DENIED]: 403,
    [AuthErrorCode.UNAUTHORIZED_CLIENT]: 403,
    [AuthErrorCode.UNSUPPORTED_GRANT_TYPE]: 400,
    [AuthErrorCode.UNSUPPORTED_RESPONSE_TYPE]: 400,
    [AuthErrorCode.TEMPORARILY_UNAVAILABLE]: 503
  };

  constructor(message: string, code?: string, uri?: string, status?: number) {
    super(message);
    const codeMapping = this.getCodeMapping();
    if (!status && code) {
      status = codeMapping[code];
    }
    this.code = code ?? 'server_error';
    this.uri = uri;
    this.status = status ?? 500;
  }

  protected getCodeMapping(): {[code: string]: number} {
    return AuthError.CODE_MAPPING;
  }
}
