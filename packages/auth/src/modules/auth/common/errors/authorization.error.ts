import {AuthErrorCode} from './constants';
import {AuthError} from './auth.error';

export class AuthorizationError extends AuthError {
  name = 'AuthorizationError';

  static readonly CODE_MAPPING: {[code: string]: number} = {
    [AuthErrorCode.INVALID_TOKEN]: 401,
    [AuthErrorCode.INVALID_REQUEST]: 401,
    [AuthErrorCode.INVALID_CLIENT]: 401,
    [AuthErrorCode.INVALID_GRANT]: 403,
    [AuthErrorCode.INVALID_SCOPE]: 401,
    [AuthErrorCode.UNAUTHENTICATED]: 401,
    [AuthErrorCode.ACCESS_DENIED]: 403,
    [AuthErrorCode.UNAUTHORIZED_CLIENT]: 403,
    [AuthErrorCode.UNSUPPORTED_GRANT_TYPE]: 401,
    [AuthErrorCode.UNSUPPORTED_RESPONSE_TYPE]: 401,
    [AuthErrorCode.TEMPORARILY_UNAVAILABLE]: 503
  };

  constructor(message: string, code?: string, uri?: string, status?: number) {
    super(message, code, uri, status);
    Error.captureStackTrace(this);
  }

  protected getCodeMapping(): {[code: string]: number} {
    return AuthorizationError.CODE_MAPPING;
  }
}
