export class BadRequestError extends Error {
  status: number;

  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this);
    this.name = 'BadRequestError';
    this.message = message;
    this.status = 400;
  }
}
