export enum AuthErrorCode {
  INVALID_TOKEN = 'invalid_token',
  INVALID_REQUEST = 'invalid_request',
  INVALID_CLIENT = 'invalid_client',
  INVALID_SCOPE = 'invalid_scope',
  INVALID_GRANT = 'invalid_grant',
  INVALID_ARGUMENT = 'invalid_argument',
  UNAUTHORIZED_CLIENT = 'unauthorized_client',
  UNAUTHENTICATED = 'unauthenticated',
  ACCESS_DENIED = 'access_denied',
  UNSUPPORTED_RESPONSE_TYPE = 'unsupported_response_type',
  UNSUPPORTED_GRANT_TYPE = 'unsupported_grant_type',
  UNSUPPORTED_TOKEN_TYPE = 'unsupported_token_type',
  TEMPORARILY_UNAVAILABLE = 'temporarily_unavailable'
}
