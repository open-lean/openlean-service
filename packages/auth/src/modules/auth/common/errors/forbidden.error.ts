export class ForbiddenError extends Error {
  status: number;

  constructor(message: string) {
    super(message);
    Error.captureStackTrace(this);
    this.name = 'ForbiddenError';
    this.message = message;
    this.status = 403;
  }
}
