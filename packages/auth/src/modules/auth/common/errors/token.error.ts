import {AuthError} from './auth.error';

export class TokenError extends AuthError {
  name = 'TokenError';

  constructor(message: string, code?: string, uri?: string, status?: number) {
    super(message, code, uri, status);
    Error.captureStackTrace(this);
  }
}
