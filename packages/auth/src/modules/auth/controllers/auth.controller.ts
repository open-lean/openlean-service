import {api, post, RestBindings, Request, Response, del} from '@loopback/rest';
import {inject} from '@loopback/context';
import {useCase} from '../../../core/decorators';
import {AuthTokenHandler} from '../usecases/token/auth-token.handler';
import {OPERATION_SECURITY_SPEC} from '../utils/security-spec';
import {TokenServiceBindings} from '../keys';
import {AuthToken} from '../../../core/models';
import {RevokeTokenHandler} from '../usecases/revoke/revoke-token.handler';
import {authenticate} from '@loopback/authentication';

const PostTokenSchema: any = {
  type: 'object',
  required: ['grantType'],
  properties: {
    grantType: {
      type: 'string'
    },
    email: {
      type: 'string'
    },
    password: {
      type: 'string'
    },
    refreshToken: {
      type: 'string'
    }
  }
};

@authenticate('jwt')
@api({basePath: '/auth'})
export class AuthController {
  constructor(
    @useCase() private tokenHandler: AuthTokenHandler,
    @useCase() private revokeTokenHandler: RevokeTokenHandler
  ) {}

  @post('/token', {
    requestBody: {
      content: {
        'application/x-www-form-urlencoded': {
          schema: PostTokenSchema
        },
        'application/json': {
          schema: PostTokenSchema
        }
      }
    },
    responses: {
      '200': {
        description: 'Get accessToken by the given credentials',
        content: {
          'application/json': {
            schema: {
              type: 'object',
              properties: {
                accessToken: {
                  type: 'string'
                },
                refreshToken: {
                  type: 'string'
                },
                tokenType: {
                  type: 'string'
                },
                expiredAt: {
                  type: 'string',
                  format: 'date-time'
                }
              }
            }
          }
        }
      }
    }
  })
  @authenticate({strategy: 'jwt', skip: true})
  async token(
    @inject(RestBindings.Http.REQUEST) request: Request,
    @inject(RestBindings.Http.RESPONSE) response: Response
  ) {
    await this.tokenHandler.invoke(request, response);
  }

  @del('/revoke', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '204': {
        description: 'Revoke the given accessToken'
      }
    }
  })
  async revoke(
    @inject(TokenServiceBindings.CURRENT_ACCESS_TOKEN) currentToken: AuthToken
  ): Promise<void> {
    await this.revokeTokenHandler.invoke(currentToken);
  }
}
