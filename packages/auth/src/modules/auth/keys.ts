import {BindingKey} from '@loopback/context';
import {PasswordHasher} from './services';
import {AuthToken} from '../../core/models';
import {AuthComponent} from './auth.component';

export namespace ScopeConstants {
  export const RESET_PASSWORD = 'reset-password';
}

export namespace TokenServiceConstants {
  export const TOKEN_EXPIRES_IN_VALUE = 1209600;
}

export namespace TokenServiceBindings {
  export const TOKEN_SECRET = BindingKey.create<string>(
    'authentication.jwt.secret'
  );
  export const TOKEN_EXPIRES_IN = BindingKey.create<number>(
    'authentication.jwt.expires.in.seconds'
  );
  export const CURRENT_ACCESS_TOKEN = BindingKey.create<AuthToken>(
    'authentication.current.accesstoken'
  );
}

export namespace PasswordHasherBindings {
  export const PASSWORD_HASHER = BindingKey.create<PasswordHasher>(
    'services.hasher'
  );
  export const ROUNDS = BindingKey.create<number>('services.hasher.round');
}

export namespace AuthBinding {
  export const COMPONENT = BindingKey.create<AuthComponent>('components.AuthComponent');
  export const CONFIG = BindingKey.buildKeyForConfig<AuthComponent>(COMPONENT.key);
}
