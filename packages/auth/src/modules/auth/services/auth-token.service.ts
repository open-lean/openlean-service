import {repository} from '@loopback/repository';
import {AuthTokenRepository, UserRepository} from '../../../core/repositories';
import {AuthToken} from '../../../core/models';
import {ATokenExtraData} from '../types';
import {UnauthorizedException} from '../../../common/exception';
import {AccessTokenMsg, ErrorMsg} from '../../../common/constants';
import {inject} from '@loopback/context';
import {TokenServiceBindings} from '../keys';
import {Setter} from '@loopback/core';
import {SystemConstant} from '../../../common/constants';
import {getLoggerFromFilename} from '../../../common/logging';

export const TOKEN_TTL = 604800;

const logger = getLoggerFromFilename(__filename, {tags: ['service', 'authToken', 'AuthTokenService']});

export class AuthTokenService {

  constructor(
    @inject.setter(TokenServiceBindings.CURRENT_ACCESS_TOKEN) readonly setCurrentToken: Setter<AuthToken>,
    @repository(AuthTokenRepository) protected accessTokenRepository: AuthTokenRepository,
    @repository(UserRepository) protected userRepository: UserRepository
  ) {
  }

  async createAuthToken(accessToken: string, userId: number, extraData?: ATokenExtraData): Promise<AuthToken> {
    const data = {
      id: accessToken,
      expiredAt: new Date(Date.now() + TOKEN_TTL * 1000),
      userId,
      ttl: TOKEN_TTL,
      issuedAt: new Date(),
      ...extraData
    };
    let authToken = new AuthToken(data);
    authToken = await this.accessTokenRepository.create(authToken);
    return authToken;
  }

  async resolve(id: string): Promise<AuthToken> {
    const accessToken = await this.accessTokenRepository.findById(id);
    const isValid = await accessToken.validate();
    if (accessToken.serviceType !== SystemConstant.SERVICE_TYPE || !isValid) {
      throw new UnauthorizedException(AccessTokenMsg.INVALID_TOKEN);
    }
    const user = await this.userRepository.findById(accessToken.userId);
    if (!user) {
      throw new UnauthorizedException(ErrorMsg.NOT_EXIST('User'));
    }
    if (user.disabled) {
      throw new UnauthorizedException(ErrorMsg.IS_DEACTIVATED('user'));
    }
    this.setCurrentToken(accessToken);
    return accessToken;
  }

  async deleteToken(token: string): Promise<void> {
    await this.accessTokenRepository.deleteById(token);
  }

  async deleteAllTokensByUserId(userId: number): Promise<void> {
    const result = await this.accessTokenRepository.deleteAll({userId});
    logger.info(`Deleted ${result.count} tokens of user ${userId}`);
  }
}