import {UserService} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {securityId} from '@loopback/security';
import {UserPermissionRepository, UserRepository} from '../../../core/repositories';
import {AuthToken, RoleMapping, User} from '../../../core/models';
import {PasswordHasher} from './hash.password.bcryptjs';
import {PasswordHasherBindings, ScopeConstants} from '../keys';
import {Credentials, AuthUserProfile} from '../types';
import {JwtService} from './jwt.service';
import {RoleTypes, ValidationConstant} from '../../../common/constants';
import {ForbiddenException, UnauthorizedException, UnprocessableEntityException} from '../../../common/exception';
import {getLoggerFromFilename} from '../../../common/logging';
import {AccountMsg, ErrorMsg} from '../../../common/constants';
import {AuthTokenService} from './auth-token.service';
import {isValidEmail} from '../../../common/utils';
import _ from 'lodash';

const logger = getLoggerFromFilename(__filename, {tags: ['users', 'account']});

function validatePassword(password: string) {
  if (!password) {
    throw new UnprocessableEntityException(ErrorMsg.IS_REQUIRED('password'));
  }

  // Bcrypt only supports up to 72 bytes; the rest is silently dropped.
  const len = Buffer.byteLength(password, 'utf8');
  if (len > ValidationConstant.MAX_PASSWORD_LENGTH) {
    throw new UnprocessableEntityException(AccountMsg.PASSWORD_TOO_LONG(len));
  }
  if (len < ValidationConstant.MIN_PASSWORD_LENGTH) {
    throw new UnprocessableEntityException(AccountMsg.PASSWORD_TOO_SHORT(len));
  }
}

export class AuthService implements UserService<User, Credentials> {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @repository(UserPermissionRepository) private userPermissionRepository: UserPermissionRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) private passwordHasher: PasswordHasher,
    @service(JwtService) private jwtService: JwtService,
    @service(AuthTokenService) private authTokenService: AuthTokenService,
  ) {
  }

  async verifyCredentials(credentials: Credentials): Promise<User> {
    const foundUser = await this.userRepository.findOne({
      where: {email: credentials.email}
    });
    if (!foundUser) {
      throw new UnauthorizedException(ErrorMsg.IS_INVALID('email'));
    }

    const passwordMatched = await this.passwordHasher.comparePassword(
      credentials.password,
      foundUser.password
    );

    if (!passwordMatched) {
      throw new UnauthorizedException(ErrorMsg.IS_INVALID('password'));
    }
    if (!foundUser.emailVerified) {
      throw new UnauthorizedException(AccountMsg.NOT_ACTIVATED);
    }
    return foundUser;
  }

  convertToUserProfile(user: User): AuthUserProfile {
    let roles: string[] = [];
    if (user.roleMappings && user.roleMappings.length > 0) {
      roles = user.roleMappings.map((item: RoleMapping) => item.roleName);
    }
    return {
      [securityId]: String(user.id),
      email: user.email,
      id: user.id ?? 0,
      companyId: user.companyId ?? 0,
      roles: roles,
      permissions: []
    };
  }

  async login(credentials: Credentials): Promise<string> {
    // ensure the user exists, and the password is correct
    const user = await this.verifyCredentials(credentials);
    if (user.disabled) {
      throw new ForbiddenException(ErrorMsg.IS_DEACTIVATED('User'));
    }

    // convert a User object into a UserProfile object (reduced set of properties)
    const userProfile = this.convertToUserProfile(user);
    userProfile.roles = await this.userRepository.getRoles(user.id);

    // create a JSON Web Token based on the user profile
    const token = await this.jwtService.generateToken(userProfile);
    logger.info(`User login successfully by email, ${userProfile.email}`);
    return token;
  }

  async logout(accessToken: AuthToken): Promise<void> {
    if (accessToken.id) {
      await this.authTokenService.deleteToken(accessToken.id);
    }
  }

  // async resetPassword(email: string): Promise<void> {
  //   isValidEmail(email);
  //   logger.info(`Reset password for a user with email: ${email}`);
  //   const user = await this.userRepository.findOne({where: {email}});
  //   if (isActiveUser(user)) {
  //     const accessToken = await this.accessTokenService.createAuthToken(user?.id ?? 0, {
  //       ttl: 43200,
  //       scopes: ScopeConstants.RESET_PASSWORD,
  //       companyId: user?.companyId
  //     });
  //     const token = await this.jwtService.signToken({
  //       userId: user?.id,
  //       email: user?.email,
  //       token: accessToken.id
  //     });
  //     const url = Settings.FRONT_END_ADDRESS + '/reset-password';
  //     const html = '<p>' + 'Hi ' + user?.email + ',' + '</p>' +
  //       '<p>Need to reset your Kt-Pulse password? Click here: <a href="' + url + '?token=' + token + '">' + url + '</a>. This link will expire in 12 hours.</p>' +
  //       '<p>If you think you received this email by mistake, please ignore it.</p>' +
  //       '<p>Thanks, <br>The Kyzentree Team</p>';
  //     const subject = 'Password reset instructions';
  //     await this.mailService.putToQueue({
  //       to: user?.email,
  //       subject,
  //       html
  //     });
  //     logger.info(`Success to init reset password: ${email}`);
  //   }
  // }

  // async setPassword(userId: number, password: string, isSendMail?: boolean): Promise<void> {
  //   validatePassword(password);
  //   // encrypt the password
  //   const encryptPassword = await this.passwordHasher.hashPassword(password);
  //   await this.userRepository.updateAttributesById(userId, {password: encryptPassword});
  //   await this.authTokenService.deleteAllTokensByUserId(userId);
  //   const user = await this.userRepository.findById(userId);
  //   const html = '<p>' + 'The password for your Kt-Pulse account ' + user.email + ' has been successfully changed. ' +
  //     'If you did not request this change, please contact support immediately: support@kyzentree.com.' + '</p>' +
  //     '<p>Thanks, <br>The Kyzentree Team</p>';
  //   const subject = 'Your Kt-Pulse password has been successfully changed';
  //   if (isSendMail) {
  //     await this.mailService.putToQueue({
  //       to: user?.email,
  //       subject,
  //       html
  //     });
  //   }
  //   logger.info(`Reset password successfully for user id: ${userId}`);
  // }

  private async _hasPassword(user: User, plain: string): Promise<boolean> {
    let isMatch = false;
    if (user.password && plain) {
      isMatch = await this.passwordHasher.comparePassword(plain, user.password);
    }
    return isMatch;
  }

  async getProfile(userId: number): Promise<User> {
    return this.userRepository.findById(userId, {
      include: [{relation: 'roleMappings'}, {relation: 'userPermissions'}, {relation: 'company'}]
    });
  }

  private validateCredentials(credentials: Credentials) {
    // Validate Email
    isValidEmail(credentials.email);

    // Validate Password
    validatePassword(credentials.password);
  }

  async createAdmin(newUser: User, role: string): Promise<User> {
    const savedUser = await this._createUser(newUser);
    if (role !== RoleTypes.KT_ADMIN && role !== RoleTypes.KT_ADMIN_READ) {
      role = RoleTypes.KT_MANAGER;
    }

    // set the password
    await this.userRepository
      .roleMappings(savedUser.id)
      .create({roleName: role});
    return savedUser;
  }

  private async _createUser(newUser: User): Promise<User> {
    // ensure a valid email value and password value
    this.validateCredentials(_.pick(newUser, ['email', 'password']));

    // encrypt the password
    newUser.password = await this.passwordHasher.hashPassword(
      newUser.password
    );

    // create the new user
    const savedUser = await this.userRepository.create(newUser);
    logger.info(`DONE create new user ${JSON.stringify(newUser)}`);
    return savedUser;
  }
}
