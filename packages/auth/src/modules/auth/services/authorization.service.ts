import {repository} from '@loopback/repository';
import _ from 'lodash';
import {RoleMappingRepository, UserPermissionRepository} from '../../../core/repositories';
import {UserPermission} from '../../../core/models';
import {AuthUserProfile} from '../types';
import {ForbiddenException} from '../../../common/exception';
import {ErrorMsg} from '../../../common/constants';

export class AuthorizationService {
  constructor(
    @repository(RoleMappingRepository) private roleMappingRepository: RoleMappingRepository,
    @repository(UserPermissionRepository) private userPermissionRepository: UserPermissionRepository
  ) {
  }

  async getPermsWithType(userId: number, modelType: string): Promise<UserPermission[]> {
    const perms = await this.userPermissionRepository.find({
      where: {
        userId,
        modelType
      }
    });
    return perms;
  }

  async getAllPermsByUserId(userId: number): Promise<string[]> {
    const perms = await this.userPermissionRepository.find({
      where: {userId}
    });
    return perms.map(item => item.scope);
  }

  async hasRoles(userId: number, roles: string[]): Promise<boolean> {
    let result = false;
    if (!Array.isArray(roles) || roles.length === 0) {
      result = true;
    }
    if (userId) {
      const userRoles = await this.roleMappingRepository.find({
        where: {
          userId
        }
      });
      const roleList = userRoles.map(item => item.roleName);
      const isMatch = _.intersection(roleList, roles);
      if (isMatch.length > 0) {
        result = true;
      }
    }
    return result;
  }

  async hasPermissions(userId: number, modelId: number, modelType: string, permissions: string[]): Promise<boolean> {
    let result = false;
    if (!Array.isArray(permissions) || permissions.length === 0) {
      result = true;
    }
    if (userId && modelId && modelType) {
      const userPerms = await this.userPermissionRepository.find({
        where: {
          modelType,
          modelId,
          userId
        }
      });
      const permList = userPerms.map(item => item.scope);
      const isMatch = _.intersection(permList, permissions);
      if (isMatch.length > 0) {
        result = true;
      }
    }
    return result;
  }

  hasCompanyPermissions(user: AuthUserProfile, permissions: string[], raiseException?: boolean): boolean {
    const matches = _.intersection(user.permissions, permissions);
    const result = Boolean(matches.length > 0);
    if (raiseException) {
      throw new ForbiddenException(ErrorMsg.NO_PERMISSION);
    }
    return result;
  }
}