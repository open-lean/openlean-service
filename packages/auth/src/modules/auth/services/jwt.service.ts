import {inject} from '@loopback/context';
import {promisify} from 'util';
import {TokenService} from '@loopback/authentication';
import {securityId} from '@loopback/security';
import {TokenServiceBindings} from '../keys';
import {service} from '@loopback/core';
import {AuthTokenService} from './auth-token.service';
import {UnauthorizedException} from '../../../common/exception';
import {AuthUserProfile} from '../types';
import {AccessTokenMsg} from '../../../common/constants';
import {AuthorizationService} from './authorization.service';
import _ from 'lodash';
import uid2 from 'uid2';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);
const verifyAsync = promisify(jwt.verify);
export const TOKEN_LENGTH = 32;

export class JwtService implements TokenService {
  constructor(
    @inject(TokenServiceBindings.TOKEN_SECRET) private jwtSecret: string,
    @inject(TokenServiceBindings.TOKEN_EXPIRES_IN) private jwtExpiresIn: number,
    @service(AuthTokenService) private authTokenService: AuthTokenService,
    @service(AuthorizationService) private authorizationService: AuthorizationService
  ) {
  }

  async verifyToken(token: string): Promise<AuthUserProfile> {
    if (!token) {
      throw new UnauthorizedException(`Error verifying token : 'token' is null`);
    }
    let userDetail: AuthUserProfile;

    try {
      // decode user profile from token
      const decodedToken = await verifyAsync(token, this.jwtSecret);

      const accessToken = await this.authTokenService.resolve(decodedToken.token);
      if (accessToken.userId !== decodedToken.userId || accessToken.updatedOn > new Date(
        decodedToken.iat * 1000 + 30000) || !accessToken.companyId) {
        throw new Error(AccessTokenMsg.INVALID_TOKEN);
      }
      const permissions = await this.authorizationService.getAllPermsByUserId(accessToken.userId);
      // don't copy over  token field 'iat' and 'exp', nor 'email' to user profile
      userDetail = Object.assign(
        {[securityId]: '', email: ''},
        {
          [securityId]: String(decodedToken.userId),
          email: decodedToken.email,
          id: decodedToken.userId,
          companyId: accessToken.companyId,
          roles: decodedToken.roles,
          permissions: _.uniq(permissions),
        }
      );
    } catch (error) {
      throw new UnauthorizedException(`Error verifying token : ${error.message}`);
    }
    return userDetail;
  }

  async generateToken(userDetail: AuthUserProfile): Promise<string> {
    if (!userDetail) {
      throw new UnauthorizedException('Error generating token : userDetail is null');
    }
    const id = uid2(TOKEN_LENGTH);
    const credentialInfo = {
      userId: userDetail.id,
      email: userDetail.email,
      roles: userDetail.roles,
      token: id
    };

    try {
      // Generate a JSON Web Token
      const token = await signAsync(credentialInfo, this.jwtSecret, {
        algorithm: 'HS256',
        expiresIn: this.jwtExpiresIn
      });
      await this.authTokenService.createAuthToken(id, userDetail.id,
        {companyId: userDetail.companyId});
      return token;
    } catch (error) {
      throw new UnauthorizedException(`Error encoding token : ${error}`);
    }
  }
}
