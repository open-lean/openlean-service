import {
  asAuthStrategy,
  AuthenticationStrategy
} from '@loopback/authentication';
import {bind} from '@loopback/context';
import {
  asSpecEnhancer,
  mergeSecuritySchemeToSpec,
  OASEnhancer,
  OpenApiSpec
} from '@loopback/openapi-v3';
import {Request} from '@loopback/rest';
import {service} from '@loopback/core';
import {JwtService} from '../services';
import {UnauthorizedException} from '../../../common/exception';
import {AuthUserProfile} from '../types';

@bind(asAuthStrategy, asSpecEnhancer)
export class JWTAuthenticationStrategy
  implements AuthenticationStrategy, OASEnhancer {
  name = 'jwt';

  constructor(@service(JwtService) private tokenService: JwtService) {
  }

  async authenticate(request: Request): Promise<AuthUserProfile | undefined> {
    const token: string = this.extractCredentials(request);
    const userDetail: AuthUserProfile = await this.tokenService.verifyToken(token);
    return userDetail;
  }

  extractCredentials(request: Request): string {
    if (!request.headers.authorization) {
      throw new UnauthorizedException(`Authorization header not found.`);
    }

    // for example : Bearer xxx.yyy.zzz
    const authHeaderValue = request.headers.authorization;

    if (!authHeaderValue.startsWith('Bearer')) {
      throw new UnauthorizedException(`Authorization header is not of type 'Bearer'.`);
    }

    //split the string into 2 parts : 'Bearer ' and the `xxx.yyy.zzz`
    const parts = authHeaderValue.split(' ');
    if (parts.length !== 2)
      throw new UnauthorizedException(
        `Authorization header value has too many parts. It must follow the pattern: 'Bearer xx.yy.zz' where xx.yy.zz is a valid JWT token.`
      );
    const token = parts[1];

    return token;
  }

  modifySpec(spec: OpenApiSpec): OpenApiSpec {
    return mergeSecuritySchemeToSpec(spec, this.name, {
      type: 'http',
      scheme: 'bearer',
      bearerFormat: 'JWT'
    });
  }
}
