import {UserProfile} from '@loopback/security';

export type Credentials = {
  email: string;
  password: string;
};

export type ATokenExtraData = {
  companyId?: number;
  scopes?: string;
  ttl?: number;
  refreshToken?: string;
}

export interface AuthUserProfile extends UserProfile {
  id: number;
  companyId: number;
  roles: string[];
  permissions: string[];
}

export interface TokenResponse {
  [key: string]: any;

  accessToken: string;
  refreshToken?: string;
  tokenType?: string;
}

interface TokenOptions {
  /**
   * Lifetime of generated refresh tokens in seconds (default = 14 days)
   */
  refreshTokenLifetime?: number | undefined;

  /**
   * Require a client secret. Defaults to true for all grant types.
   */
  requireClientAuthentication?: {} | undefined;

  /**
   * Always revoke the used refresh token and issue a new one for the refresh_token grant.
   */
  alwaysIssueNewRefreshToken?: boolean | undefined;

  /**
   * For maximum flexibility, multiple scope spearators can optionally be allowed.
   * This allows the server to accept clients that separate scope with either space or comma (' ', ',').
   * This violates the specification, but achieves compatibility with existing client libraries that are already deployed.
   */
  scopeSeparator?: string | undefined;
}

interface AuthenticateOptions {
  /**
   * Set the X-Accepted-OAuth-Scopes HTTP header on response objects.
   */
  addAcceptedScopesHeader?: boolean | undefined;

  /**
   * Set the X-OAuth-Scopes HTTP header on response objects.
   */
  addAuthorizedScopesHeader?: boolean | undefined;
}

interface AuthorizeOptions {
  /**
   * Allow clients to specify an empty state
   */
  allowEmptyState?: boolean | undefined;

  /**
   * Lifetime of generated authorization codes in seconds (default = 5 minutes).
   */
  authorizationCodeLifetime?: number | undefined;
}

export interface AuthOptions {
  authenticateOptions: AuthenticateOptions;
  authorizeOptions: AuthorizeOptions;
  tokenOptions: TokenOptions;
}