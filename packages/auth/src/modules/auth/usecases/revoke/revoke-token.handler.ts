import {IUseCase} from '../../../../core/abstracts';
import {injectable, BindingScope, service} from '@loopback/core';
import {AuthService} from '../../services';
import {AuthToken} from "../../../../core/models";

@injectable({scope: BindingScope.SINGLETON})
export class RevokeTokenHandler implements IUseCase<void> {
  constructor(@service() private authService: AuthService) {}

  /**
   * To handle revoke token logic
   * @param [authToken] The access token string
   */
  async invoke(authToken: AuthToken): Promise<void> {
    await this.authService.logout(authToken);
  }
}
