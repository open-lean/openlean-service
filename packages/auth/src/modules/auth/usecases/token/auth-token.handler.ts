import {IUseCase} from '../../../../core/abstracts';
import {inject, injectable, BindingScope, service} from '@loopback/core';
import _ from 'lodash';
import {SecretKeyService} from '../../../../services/secretkey';
import {AuthErrorCode, TokenError} from '../../common/errors';
import {AuthOptions, TokenResponse} from '../../types';
import {IsValidator} from '../../utils/is-validator';
import {PasswordGrantType} from './exchanges/password';
// import {RefreshTokenGrantType} from './exchanges/refresh-token';
import {Request, Response} from '@loopback/rest';
import {AbstractGrantType} from './exchanges/abstract';
import {AuthService} from '../../services';
import Settings from '../../../../settings';


const auth = require('basic-auth');

@injectable({scope: BindingScope.SINGLETON})
export class AuthTokenHandler implements IUseCase<void> {
  private readonly grantTypes: {[key: string]: AbstractGrantType};

  constructor(
    @service() private authService: AuthService,
  ) {
    // Setup Grant Types Mapping
    this.grantTypes = {
      password: new PasswordGrantType(authService),
      //refresh_token: new RefreshTokenGrantType()
      /* eslint-enable */
    };
  }

  async invoke(request: Request, response: Response): Promise<void> {
    if (request.method !== 'POST') {
      throw new TokenError('Invalid request: method must be POST', AuthErrorCode.INVALID_REQUEST);
    }

    if (!request.is('application/x-www-form-urlencoded') && !request.is('application/json')) {
      throw new TokenError(
        'Invalid request: content must be application/x-www-form-urlencoded or application/json',
        AuthErrorCode.INVALID_REQUEST
      );
    }

    // Step 1: Handle exchange token by specific grantType
    const tokenResponse = await this.handleGrantType(request);

    // Step 2: success & update response
    AuthTokenHandler.success(response, tokenResponse);
  }

  private async handleGrantType(request: Request): Promise<TokenResponse> {
    const grantType = request.body.grantType;
    if (!grantType) {
      throw new TokenError('Missing parameter: `grant_type`', AuthErrorCode.INVALID_REQUEST);
    }
    if (!IsValidator.nchar(grantType) && !IsValidator.uri(grantType)) {
      throw new TokenError('Invalid parameter: `grant_type`', AuthErrorCode.INVALID_REQUEST);
    }
    if (!_.has(this.grantTypes, grantType)) {
      throw new TokenError('Unsupported grant type: `grant_type` is invalid', AuthErrorCode.UNSUPPORTED_GRANT_TYPE);
    }

    const grantTypeHandler = this.grantTypes[grantType];
    grantTypeHandler.setJwtSecretKey(Settings.TOKEN_SECRET);
    return grantTypeHandler.handle(request);
  }

  private static success(response: Response, data: TokenResponse) {
    const json = JSON.stringify(data);
    response.statusCode = 200;
    response.set('Content-Type', 'application/json');
    response.set('Cache-Control', 'no-store');
    response.set('Pragma', 'no-cache');
    response.end(json);
  }
}
