import {AuthToken, User} from '../../../../../core/models';
import {TokenResponse} from '../../../types';
import {generateToken, clientInfo} from '../../../utils/helper';
import {Request} from '@loopback/rest';

interface AccessTokenAndRefreshToken {
  accessToken: {[key: string]: any};
  refreshToken: {[key: string]: any};
}

export abstract class AbstractGrantType {
  readonly name: string = '';
  // in seconds
  // protected readonly refreshTokenLifetime: number;
  // protected readonly scopeSeparator: string;
   protected jwtSecretKey: string;

  // protected constructor(options: {[key: string]: any}) {
  //   options = options || {};
  //
  //   this.refreshTokenLifetime = options.refreshTokenLifetime ?? 1209600; // Default: 2 weeks
  //   this.scopeSeparator = options.scopeSeparator || ' ';
  // }
  protected constructor() {
  }
  /**
   * Set jwtSecretKey
   *
   * @param [key] The given jwt secret key
   */
  setJwtSecretKey(key: string) {
    this.jwtSecretKey = key;
  }

  /**
   * Handle by Grant Type
   * @param [request]: The instance of OAuthRequest
   */
  abstract handle(request: Request): Promise<TokenResponse>;

  /**
   * To build a token response data.
   *
   * @param [accessToken] The access token string
   * @param [refreshToken] The refresh token string
   * @param [params] The additional data
   */
  protected issued(accessToken: string, refreshToken?: string, params?: {[key: string]: any}): TokenResponse {
    if (refreshToken && typeof refreshToken === 'object') {
      params = refreshToken;
      refreshToken = undefined;
    }
    const tokenResponse: TokenResponse = {
      accessToken: accessToken,
      refreshToken: refreshToken
    };
    if (params) {
      Object.assign(tokenResponse, params);
    }
    tokenResponse.tokenType = tokenResponse.token_type || 'Bearer';
    return tokenResponse;
  }

  /**
   * To generate an access token and refresh token
   *
   * @param [scopes] The list of scope
   * @param [user] The instance of User
   */
  protected async generateAccessTokenAndRefreshToken(
    scopes: string[],
    user?: User
  ): Promise<AccessTokenAndRefreshToken> {
    const accessToken = await generateToken({grant: this.name, user, scopes, secret: this.jwtSecretKey});
    console.log(`Generating access token: ${accessToken}`, scopes);

    const refreshToken = await generateToken({grant: this.name, user, scopes, refreshToken: true});
    return {accessToken, refreshToken};
  }
}
