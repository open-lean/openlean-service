import {AuthErrorCode, TokenError} from '../../../common/errors';
import {IsValidator} from '../../../utils/is-validator';
import {AbstractGrantType} from './abstract';
import {Request} from '@loopback/rest';
import {AuthService} from '../../../services';

export class PasswordGrantType extends AbstractGrantType {
  readonly name: string = 'Resource Owner Password Credentials';
  private authService: AuthService;
  constructor(authService: AuthService) {
    super();
    this.authService = authService;
  }

  /**
   * Handle resource owner password credentials grant
   * @param [request] The OAuthRequest instance
   * @param [client] The OAuthClientApplication instance
   */
  async handle(request: Request): Promise<any> {
    const identifier = request.body.email ?? request.body.username;
    const password = request.body.password;
    if (!identifier) {
      throw new TokenError('Missing required parameter: {{username}}', AuthErrorCode.INVALID_REQUEST);
    }

    if (!password) {
      throw new TokenError('Missing required parameter: {{password}}', AuthErrorCode.INVALID_REQUEST);
    }

    if (!IsValidator.uchar(identifier)) {
      throw new TokenError('Invalid parameter: {{username}}', AuthErrorCode.INVALID_REQUEST);
    }

    if (!IsValidator.uchar(password)) {
      throw new TokenError('Invalid parameter: {{password}}', AuthErrorCode.INVALID_REQUEST);
    }
    const accessToken = await this.authService.login({email: identifier, password});
    return this.issued(accessToken);
  }
}
