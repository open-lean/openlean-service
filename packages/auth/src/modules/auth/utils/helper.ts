import _ from 'lodash';
import {AuthToken, User} from '../../../core/models';
import uid2 from 'uid2';
import {promisify} from 'util';

const jwt = require('jsonwebtoken');
const signAsync = promisify(jwt.sign);

export async function generateToken(options?: {[key: string]: any}): Promise<{[key: string]: any}> {
  options = options ?? {};
  const id = uid2(32);
  const output = {id: id};
  if (options.client?.tokenType === 'jwt' && !options.refreshToken && options.secret) {
    // TODO: Provide a func to get token secret
    const secret = options.secret;
    const user = options.user;
    const payload: any = {
      appId: options.client.id,
      scopes: options.scopes
    };
    if (user) {
      payload.userId = user.id;
      payload.companyId = user.companyId;
      payload.role = user.roleMapping?.roleName;
      if (user.permissions?.length > 0) {
        payload.permissions = JSON.stringify(
          options.user.permissions.map((item: any) => _.pick(item, ['scope', 'modelId', 'modelType']))
        );
      }
    }

    const token = await signAsync(payload, secret, {
      algorithm: 'HS256',
      expiresIn: options.client.getAccessTokenLifeTime()
    });
    output.id = token;
  } else if (options.client && options.client.tokenType === 'mac') {
    // options.jwtAlgorithm = 'HS256'; // HS256 for mac token
    // TODO: Will implement for case MAC type later
  }
  return output;
}

export function clientInfo(client: any) {
  if (!client) {
    return client;
  }
  return client.id + ',' + client.name;
}

export function isExpired(tokenOrCode: AuthToken | any): boolean {
  const issuedTime = tokenOrCode.issuedAt?.getTime() ?? -1;
  const now = Date.now();
  let expirationTime = tokenOrCode.expiredAt?.getTime() ?? -1;
  if (expirationTime === -1 && issuedTime !== -1 && tokenOrCode.expiresIn > 0) {
    expirationTime = issuedTime + tokenOrCode.expiresIn * 1000;
  }
  return now > expirationTime;
}

export function buildTokenParams(accessToken: AuthToken, token: {[key: string]: any}) {
  const params: any = {
    expiredAt: accessToken.expiredAt
  };
  if (accessToken.scopes) {
    params.scope = accessToken.scopes;
  }
  if (accessToken.refreshToken) {
    params.refreshToken = accessToken.refreshToken;
  }
  if (typeof token === 'object') {
    for (const p in token) {
      if (p !== 'id' && !params[p] && token[p]) {
        params[p] = token[p];
      }
    }
  }
  return params;
}
