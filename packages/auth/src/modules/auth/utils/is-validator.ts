/**
 * Validation rules.
 */
const rules = {
  NCHAR: /^[\u002D|\u002E|\u005F|\w]+$/,
  NQCHAR: /^[\u0021|\u0023-\u005B|\u005D-\u007E]+$/,
  NQSCHAR: /^[\u0020-\u0021|\u0023-\u005B|\u005D-\u007E]+$/,
  // eslint-disable-next-line no-control-regex
  UNICODECHARNOCRLF: /^[\u0009|\u0020-\u007E|\u0080-\uD7FF|\uE000-\uFFFD|\u10000-\u10FFFF]+$/,
  URI: /^[a-zA-Z][a-zA-Z0-9+.-]+:/,
  VSCHAR: /^[\u0020-\u007E]+$/
};

export class IsValidator {
  /**
   * Validate if a value matches a unicode character.
   *
   * @see https://tools.ietf.org/html/rfc6749#appendix-A
   */
  static nchar(value: string) {
    return rules.NCHAR.test(value);
  }

  /**
   * Validate if a value matches a unicode character, including exclamation marks.
   *
   * @see https://tools.ietf.org/html/rfc6749#appendix-A
   */

  static nqchar(value: string) {
    return rules.NQCHAR.test(value);
  }

  /**
   * Validate if a value matches a unicode character, including exclamation marks and spaces.
   *
   * @see https://tools.ietf.org/html/rfc6749#appendix-A
   */

  static nqschar(value: string) {
    return rules.NQSCHAR.test(value);
  }

  /**
   * Validate if a value matches a unicode character excluding the carriage
   * return and linefeed characters.
   *
   * @see https://tools.ietf.org/html/rfc6749#appendix-A
   */

  static uchar(value: string) {
    return rules.UNICODECHARNOCRLF.test(value);
  }

  /**
   * Validate if a value matches generic URIs.
   *
   * @see http://tools.ietf.org/html/rfc3986#section-3
   */
  static uri(value: string) {
    return rules.URI.test(value);
  }

  /**
   * Validate if a value matches against the printable set of unicode characters.
   *
   * @see https://tools.ietf.org/html/rfc6749#appendix-A
   */
  static vschar(value: string) {
    return rules.VSCHAR.test(value);
  }
}
