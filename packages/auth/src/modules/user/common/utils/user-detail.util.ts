import {securityId} from '@loopback/security';
import {normalizeScope} from '../../../../common/utils';
import {User} from '../../../../core/models';
import {AuthUserProfile} from '../../../auth';


export class UserDetailUtil {
  static buildUserDetail(user: User, scopes: string[]): AuthUserProfile {
    const uniqueId: string = `user_${user.id}`;
    return {
      [securityId]: uniqueId,
      companyId: user.companyId,
      userId: user.id,
      scopes: normalizeScope(scopes)
    } as any;
  }
}
