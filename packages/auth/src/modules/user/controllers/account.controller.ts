import {useCase} from '../../../core/decorators';
import {authenticate} from '@loopback/authentication';
import {inject} from '@loopback/context';
import {api, post, requestBody} from '@loopback/rest';
import {SecurityBindings} from '@loopback/security';
import {ChangePassword} from '../usecases/changepassword';
import {AuthUserProfile, OPERATION_SECURITY_SPEC} from '../../auth';

@api({basePath: '/account'})
@authenticate('jwt')
export default class AccountController {
  constructor(@useCase() private changePassword: ChangePassword) {}

  @post('/changePassword', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Reset password for a user with email.'
      }
    }
  })
  async requestChangePassword(
    @inject(SecurityBindings.USER) userDetail: AuthUserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['oldPassword', 'newPassword'],
            properties: {
              oldPassword: {
                type: 'string'
              },
              newPassword: {
                type: 'string',
                minLength: 8,
                maxLength: 60,
                pattern: '^(?=.*[a-z])(?=.*\\d)[a-zA-Z\\d@$!%*?&_]{8,}$'
              }
            }
          }
        }
      }
    })
    payload: {oldPassword: string; newPassword: string}
  ): Promise<void> {
    await this.changePassword.invoke(userDetail.id!, payload.oldPassword, payload.newPassword);
  }
}
