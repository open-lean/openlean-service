import {useCase} from '../../../core/decorators';
import {authenticate} from '@loopback/authentication';
import {authorize} from '@loopback/authorization';
import {inject} from '@loopback/core';
import {api, post, requestBody} from '@loopback/rest';
import {SecurityBindings} from '@loopback/security';
import {APIScopes} from '../../../common/constants';
import {AuthUserProfile, OPERATION_SECURITY_SPEC} from '../../auth';
import {ResetPasswordByEmail, SetNewPassword} from '../usecases/forgotpassword';

@api({basePath: '/account'})
@authenticate('jwt')
export default class ForgotPasswordController {
  constructor(
    @useCase() private resetPasswordByEmail: ResetPasswordByEmail,
    @useCase() private setNewPassword: SetNewPassword
  ) {}

  @post('/forgotPassword/init', {
    responses: {
      '200': {
        description: 'Reset password for a user with email.'
      }
    }
  })
  @authenticate({strategy: 'auth', skip: true})
  async initForgotPassword(
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['email'],
            properties: {
              email: {
                type: 'string',
                format: 'email'
              }
            }
          }
        }
      }
    })
    payload: {
      email: string;
    }
  ): Promise<void> {
    await this.resetPasswordByEmail.invoke(payload.email);
  }

  @post('/forgotPassword/finish', {
    security: OPERATION_SECURITY_SPEC,
    responses: {
      '200': {
        description: 'Reset password for a user with email.'
      }
    }
  })
  @authorize({scopes: [APIScopes.AUTH_RESET_PASSWORD]})
  async finishForgotPassword(
    @inject(SecurityBindings.USER) userDetail: AuthUserProfile,
    @requestBody({
      content: {
        'application/json': {
          schema: {
            type: 'object',
            required: ['password'],
            properties: {
              password: {
                type: 'string',
                minLength: 8,
                maxLength: 60,
                pattern: '^(?=.*[a-z])(?=.*\\d)[a-zA-Z\\d@$!%*?&_]{8,}$'
              }
            }
          }
        }
      }
    })
      payload: {password: string}
  ): Promise<void> {
    await this.setNewPassword.invoke(userDetail.userId!, payload.password);
  }
}
