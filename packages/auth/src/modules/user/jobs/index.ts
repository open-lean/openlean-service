import {BaseApplication} from '../../../core/abstracts';
import {createBindingFromClass} from '@loopback/context';
import {SendMailJob} from './send-mail.job';

/**
 * Register all jobs binding here
 *
 * @param [app] The instance of application
 */
export function registerJobBindings(app: BaseApplication) {
  app.add(createBindingFromClass(SendMailJob));
}
