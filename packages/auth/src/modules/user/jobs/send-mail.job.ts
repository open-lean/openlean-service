import {Job} from 'bull';
import {getLoggerFromFilename} from '../../../common/logging';
import MailService from '../../../services/mail/mail.service';
import {queueJob, QueueJob} from '../../../core/configs/queue';
import {service} from "@loopback/core";

const logger = getLoggerFromFilename(__filename, { tags: ['job', 'mail'] });

@queueJob()
export class SendMailJob extends QueueJob {
  constructor(
    @service() private mailService: MailService
  ) {
    super({name: 'send-mail'});
  }

  async handle(job: Job): Promise<void> {
    console.log('It works');
    const data: any = job.data;
    await this.mailService.send(data);
  }
}