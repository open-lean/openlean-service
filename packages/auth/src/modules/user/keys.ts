import {BindingKey} from '@loopback/context';
import {UserComponent} from './user.component';

export namespace UserBinding {
  export const COMPONENT = BindingKey.create<UserComponent>('components.UserComponent');
}
