import {BadRequestException, UnprocessableEntityException} from '../../../../common/exception';
import {IUseCase} from '../../../../core/abstracts';
import {getLoggerFromFilename} from '../../../../common/logging';
import {inject} from '@loopback/context';
import {repository} from '@loopback/repository';
import {UserRepository, AuthTokenRepository} from '../../../../core/repositories';
import {PasswordHasherBindings, PasswordHasher} from '../../../auth';

const logger = getLoggerFromFilename(__filename, {tags: ['user', 'change-password']});

/**
 * The use-case of changing password for user
 */
export class ChangePassword implements IUseCase<void> {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @repository(AuthTokenRepository) private authTokenRepository: AuthTokenRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) private passwordHasher: PasswordHasher
  ) {}

  /**
   * Handle logic of changing password
   *
   * @param [userId] The ID of user
   * @param [oldPassword] The old password of user
   * @param [newPassword] The new password to change
   */
  async invoke(userId: number, oldPassword: string, newPassword: string): Promise<void> {
    const user = await this.userRepository.findById(userId);
    if (user.disabled) {
      throw new BadRequestException('The user has no longer active');
    }
    const isMatch = await this.passwordHasher.comparePassword(oldPassword, user.password);
    if (!isMatch) {
      throw new UnprocessableEntityException('Invalid current password');
    }

    const hashedPassword = await this.passwordHasher.hashPassword(newPassword);
    await this.userRepository.updateAttributesById(userId, {password: hashedPassword});
    await this.authTokenRepository.deleteAll({userId});
    logger.info(`The user ${userId} has changed password successfully`);
  }
}
