import {NotFoundException, BadRequestException} from '../../../../common/exception';
import {IUseCase} from '../../../../core/abstracts';
import {getLoggerFromFilename} from '../../../../common/logging';
import {JobConstant, JobManager} from '../../../../core/configs/queue';
import {service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {APIScopes} from '../../../../common/constants';
import {UserRepository} from '../../../../core/repositories';
import Settings from '../../../../settings';
import {AuthBinding} from '../../../auth';
import {UserDetailUtil} from '../../common/utils/user-detail.util';
import MailService from '../../../../services/mail/mail.service';

const logger = getLoggerFromFilename(__filename, {tags: ['user', 'forgot-password', 'reset-password']});

/**
 * The use-case of sending request to reset password
 */
export class ResetPasswordByEmail implements IUseCase<void> {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @service() private jobManager: JobManager
  ) {}

  /**
   * Handle logic of the use-case
   *
   * @param [email] The user email
   */
  async invoke(email: string): Promise<void> {
    const user = await this.userRepository.findOne({where: {email}});
    if (!user || user.disabled) {
      throw new NotFoundException('User may not exist');
    }
    if (!user.emailVerified) {
      throw new BadRequestException('The email has not been verified yet');
    }
    const userDetail = UserDetailUtil.buildUserDetail(user, [APIScopes.AUTH_RESET_PASSWORD]);
    const url = Settings.FRONT_END_ADDRESS + '/reset-password?token=';
    const html = '<p>' + 'Hi ' + user?.email + ',' + '</p>' +
            '<p>Need to reset your Open-Lean password? Click here: <a href="' + url + '?token=' + 'hello' + '">' + url + '</a>. This link will expire in 12 hours.</p>' +
            '<p>If you think you received this email by mistake, please ignore it.</p>' +
            '<p>Thanks, <br>The Leansoft Team</p>';

    await this.jobManager.putToQueue(JobConstant.SEND_MAIL_JOB, {
      to: user?.email,
      subject: 'Password reset instructions',
      html
    });

    logger.info(`Success to init reset password: ${email}`);
  }
}
