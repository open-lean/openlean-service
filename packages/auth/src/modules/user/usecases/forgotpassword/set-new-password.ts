import {IUseCase} from '../../../../core/abstracts';
import {getLoggerFromFilename} from '../../../../common/logging';
import {JobConstant, JobManager} from '../../../../core/configs/queue';
import {inject} from '@loopback/context';
import {service} from '@loopback/core';
import {repository} from '@loopback/repository';
import {UserRepository, AuthTokenRepository} from '../../../../core/repositories';
import {PasswordHasherBindings, PasswordHasher} from '../../../auth';

const logger = getLoggerFromFilename(__filename, {tags: ['user', 'forgot-password', 'set-password']});

/**
 * The use-case of setting new password after user has requested forget password
 */
export class SetNewPassword implements IUseCase<void> {
  constructor(
    @repository(UserRepository) private userRepository: UserRepository,
    @repository(AuthTokenRepository) private authTokenRepository: AuthTokenRepository,
    @inject(PasswordHasherBindings.PASSWORD_HASHER) private passwordHasher: PasswordHasher,
    @service() private jobManager: JobManager
  ) {}

  /**
   * Handle logic of set new password for user
   *
   * @param [userId] The ID of user
   * @param [newPassword] The new password of user
   */
  async invoke(userId: number, newPassword: string): Promise<void> {
    const user = await this.userRepository.findById(userId);
    const hashedPassword = await this.passwordHasher.hashPassword(newPassword);
    await this.userRepository.updateAttributesById(userId, {password: hashedPassword});
    await this.authTokenRepository.deleteAll({userId});
    logger.info(`Set new password for user ID: ${userId} successfully`);
    const html = '<p>' + 'The password for your Open-Lean account ' + user.email + ' has been successfully changed. ' +
      'If you did not request this change, please contact support immediately: support@leansoft.com.' + '</p>' +
      '<p>Thanks, <br>The Leansoft Team</p>';
    await this.jobManager.putToQueue(JobConstant.SEND_MAIL_JOB, {
      to: user.email,
      subject: 'Your Open-Lean password has been successfully changed',
      html
    });
  }
}
