import {Component, ContextTags, injectable} from '@loopback/core';
import AccountController from './controllers/account.controller';
import ForgotPasswordController from './controllers/forgot-password.controller';
import {UserBinding} from './keys';
import {ChangePassword} from './usecases/changepassword';
import {ResetPasswordByEmail, SetNewPassword} from './usecases/forgotpassword';

/** User component */
@injectable({tags: {[ContextTags.KEY]: UserBinding.COMPONENT}})
export class UserComponent implements Component {
  controllers = [AccountController, ForgotPasswordController];
  useCases = [
    // Forgot password
    ResetPasswordByEmail,

    // Change Password
    ChangePassword,

    SetNewPassword
  ];
}
