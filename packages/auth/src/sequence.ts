import {MiddlewareSequence, RequestContext} from '@loopback/rest';
import debugFactory from 'debug';

const debug = debugFactory('kt-pulse:rest:sequence');

export class DefaultSequence extends MiddlewareSequence {
  async handle(context: RequestContext) {
    const {request} = context;
    debug('>>> %s Request %s %s', new Date().toISOString(), request.method, request.originalUrl);
    await super.handle(context);
    debug(
      '>>> %s Response received for %s %s',
      new Date().toISOString(),
      request.method,
      request.originalUrl,
    );
  }
}
