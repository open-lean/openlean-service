import {Filter, repository} from '@loopback/repository';
import {QueueJobRepository} from '../../core/repositories';
import {getLoggerFromFilename} from '../../common/logging';
import {QueueJob, QueueJobRelations} from '../../core/models';
import {Options} from '@loopback/repository/src/common-types';

const logger = getLoggerFromFilename(__filename, { tags: ['queue', 'job'] });

export default class QueueJobService {
  constructor(
    @repository(QueueJobRepository) protected queueJobRepository: QueueJobRepository
  ){
  }

  async create(title: string, name: string, data: object|string): Promise<QueueJob> {
    if (typeof data === 'object') {
      data = JSON.stringify(data);
    }
    let queueJob = new QueueJob({title, name, data});
    queueJob = await this.queueJobRepository.create(queueJob);
    logger.info(`Create a queueJob successfully: ${queueJob.id} ${name}`);
    return queueJob;
  }

  async deleteById(id: number): Promise<void> {
    try {
      await this.queueJobRepository.deleteById(id);
      logger.info(`Deleted QueueJob by id: ${id}`);
    } catch (e) {
      logger.error({err: e}, e.message);
    }
  }

  async assignJobId(id: number, jobId: string | number): Promise<void> {
    if (typeof jobId === 'number') {
      jobId = jobId.toString();
    }
    await this.queueJobRepository.updateAttributesById(id, {jobId});
    logger.info(`Assigned jobid ${jobId} to queueJob ${id}`);
  }

  async updateStatusById(id: number, status: string, reason?: string): Promise<void> {
    await this.queueJobRepository.updateAttributesById(id, {status, reason});
    logger.info(`Updated a queueJob ${id} status to "${status}"`);
  }

  async findOne(filter?: Filter, options?: Options): Promise<(QueueJob & QueueJobRelations) | null> {
    const queueJob = await this.queueJobRepository.findOne(filter, options);
    return queueJob;
  }
}