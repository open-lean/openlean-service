import * as nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import {getLoggerFromFilename} from '../../common/logging';
import {service} from '@loopback/core';
import {JobManager, JobConstant} from '../../core/configs/queue';
import Settings from '../../settings';

const logger = getLoggerFromFilename(__filename, {tags: ['mail']});

export interface EmailPayload extends Mail.Options {
  auth?: any
}

export default class MailService {
  constructor(
    @service(JobManager) private jobManager: JobManager
  ) {
  }

  async send(payload: EmailPayload): Promise<void> {
    payload.from = `"OpenLean Bot" <${Settings.EMAIL}>`;
    const transporter: Mail = nodemailer.createTransport({
      host: 'smtp.gmail.com',
      port: 465,
      secure: true,
      tls: {
        rejectUnauthorized: false
      },
      auth: {
        type: 'OAuth2',
        user: Settings.EMAIL,
        clientId: Settings.EMAIL_CLIENT_ID,
        clientSecret: Settings.EMAIL_CLIENT_SECRET,
        refreshToken: Settings.EMAIL_REFRESH_TOKEN
      }
    });
    payload.from = Settings.EMAIL;
    await transporter.sendMail(payload);
    logger.info(`Sent an email to ${payload.to}`);
  }

  async putToQueue(payload: EmailPayload): Promise<void> {
    await this.jobManager.putToQueue(JobConstant.SEND_MAIL_JOB, payload);
  }
}