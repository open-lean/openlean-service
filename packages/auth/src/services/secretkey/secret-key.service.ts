import {injectable, BindingScope} from '@loopback/core';
import {repository} from '@loopback/repository';
import crypto from 'crypto';
import {SecretKey} from '../../core/models';
import {SecretKeyRepository} from '../../core/repositories';

@injectable({scope: BindingScope.SINGLETON})
export class SecretKeyService {
  constructor(@repository(SecretKeyRepository) private secretKeyRepository: SecretKeyRepository) {}

  /**
   * Get or create new secret key
   */
  async getOrCreateSecretKey(): Promise<SecretKey> {
    const now = new Date();
    let secretKey = await this.secretKeyRepository.findOne({
      where: {
        expiredOn: {gt: now}
      }
    });

    if (!secretKey) {
      const key = crypto.randomBytes(8).toString('hex');
      const nextMonth = new Date(now.setMonth(now.getMonth() + 1));
      secretKey = await this.secretKeyRepository.create({
        key,
        expiredOn: nextMonth
      });
    }

    return secretKey;
  }

  /**
   * Get list of secret keys which are in used
   */
  async getInUsedSecretKeys(): Promise<SecretKey[]> {
    return this.secretKeyRepository.find({order: ['expiredOn DESC'], limit: 2});
  }
}
