const Settings: any = {};

export function loadSettings() {
  Settings.HOST = process.env.HOST;
  Settings.PORT = process.env.PORT;
  Settings.NODE_ENV = process.env.NODE_ENV ?? 'local';
  Settings.TOKEN_SECRET = process.env.TOKEN_SECRET;
  Settings.DEFAULT_TIMEZONE = process.env.TZ ?? 'UTC';

  // MYSQL settings
  Settings.MYSQL_URL = process.env.MYSQL_URL;
  Settings.MYSQL_HOST = process.env.MYSQL_HOST;
  Settings.MYSQL_PORT = process.env.MYSQL_PORT;
  Settings.MYSQL_USER = process.env.MYSQL_USER;
  Settings.MYSQL_PASSWORD = process.env.MYSQL_PASSWORD;
  Settings.MYSQL_DATABASE = process.env.MYSQL_DATABASE;

  // Redis settings
  Settings.REDIS_CONNECTION_URL = process.env.REDIS_CONNECTION_URL;
  Settings.REDIS_HOST = process.env.REDIS_HOST;
  Settings.REDIS_PORT = process.env.REDIS_PORT;
  Settings.REDIS_SENTINEL_PORT = process.env.REDIS_PORT;
  Settings.REDIS_PASSWORD = process.env.REDIS_PASSWORD;

  // Mail Settings
  Settings.EMAIL = process.env.EMAIL;
  Settings.EMAIL_PASSWORD = process.env.EMAIL_PASSWORD;
  Settings.EMAIL_CLIENT_ID = process.env.EMAIL_CLIENT_ID;
  Settings.EMAIL_CLIENT_SECRET = process.env.EMAIL_CLIENT_SECRET;
  Settings.EMAIL_REFRESH_TOKEN = process.env.EMAIL_REFRESH_TOKEN;

  // Elasticsearch Settings
  Settings.ELASTICSEARCH_USERNAME = process.env.ELASTICSEARCH_USERNAME;
  Settings.ELASTICSEARCH_PASSWORD = process.env.ELASTICSEARCH_PASSWORD;
  Settings.ELASTICSEARCH_URLS = process.env.ELASTICSEARCH_URLS;

  // Front end Settings
  const host = process.env.FRONT_END_HOST;
  const port = process.env.FRONT_END_PORT;
  const protocol = process.env.FRONT_END_PROTOCOL;
  const displayPort = ((protocol === 'http' && port === '80') ||
    (protocol === 'https' && port === '443')) ? '' : ':' + port;
  Settings.FRONT_END_ADDRESS = `${protocol}://${host}${displayPort}`;

  // CORS configuration
  // Example: ['http://example.com'] or '*'
  Settings.CORS_ORIGIN = '*';
  if (process.env.CORS_ORIGIN) {
    Settings.CORS_ORIGIN = process.env.CORS_ORIGIN.split(' ');
  }
}

export default Settings;